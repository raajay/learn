## Salient Features of Scala

1. Every thing is an object, including functions and literals

2. Thus function, can be passed as values to a function.

3. "object HelloWorld {}" : such a definition represents a singleton object.
   Essentially, this can be viewed as a class definition and an instantiation of
   that class. There can be only one such object ever. No more instances of
   class with similar structure can exist

4. An object (created as instantiation of a class) can contain member variables
   and function. But then here functions and variables are all _objects_. Thus,
   an object can contain multiple other objects in its scope.

5. Objects have DataTypes associated with them. A few examples are Int, String.
   We associate every object with a data type. A object which is a function has
   the following type of identifier: "(Argument_Identifiers) => Return_Type"

6. Functions that are used only once need not have a distinct name. This is
   because, they can be defined when they are going to be used. Such functions
   are called Anonymous functions.

7. Classes can be defined. They can have parameters; which means they have only
   one constructor. Classes are used to create new objects using the "new"
   keyword. Classes can have methods.

8. Return types of function are generally inferred by the compiler. In cases,
   where the types cannot be inferred, compiler errors are thrown.

9. The body of functions should be defined with a "=" command, i.e., an equal to
   symbol is required between the function identifier and the body of the
   function. However, for an anonymous function, we used the following
   convention, () => {--body--}. The last statement

10. By default every object has a "toString" method defined. We can use the
    override it to change the functionality.

11. Case class - specific to Scala. Usually extends an abstract class.

12. A new keyword is not required to instantiate an object of the case class.
    Getter functions are automatically defined for the constructor parameters.
    "equals" and "hashCode" methods are automatically defined.

13. A "toString" function that prints the value in source form is defined.
    Instances of classes can be decomposed through pattern matching.
