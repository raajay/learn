abstract class Tree

case class Sum(left: Tree, right: Tree) extends Tree
case class Var(v: String) extends Tree
case class Const(c: Int) extends Tree


object CaseTest {

  type Environment = String => Int

  def eval(t: Tree, env: Environment) : Int = t match {
    case Sum(left, right) => eval(left, env) + eval(right, env)
    case Var(v) => env(v)
    case Const(c) => c
  }

  def simplify(t: Tree): Tree = t match {
    case Var(v) => t
    case Const(c) => t
    case Sum(l, r) =>
      val sl = simplify(l)
      val sr = simplify(r)

      val new_s = Sum(sl, sr)
      new_s match {
        case Sum(Const(a), Const(b)) => Const(a+b)
        case _ => new_s
      }
  }

  def main(args: Array[String]) {
    val x: Tree = Sum(Sum(Var("x"),Var("x")), Sum(Const(1), Const(2)))
    val y: Tree = Sum(Const(0), Sum(Const(1), Sum(Const(2), Const(3))))
    val z: Tree = Sum(Sum(Var("x"), Const(1)), Sum(Var("x"), Const(1)))
    val env: Environment = {case "x" => 3}

    println(simplify(x))
    println(simplify(y))
    println(simplify(z))

    //println(eval(x, env))
  }

}
