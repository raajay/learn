class Complex(real: Double, imaginary:Double) {
  def re() = real
  def im() = imaginary

  override def toString = {
    "" + "re: " + re + ", im: " + im
  }
}

object ComplexTest {
  def main(args: Array[String]) {
    var c = new Complex(3, 4)
    println(c.re)
    println(c.toString)
  }
}
