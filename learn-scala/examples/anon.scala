object Test {
  def oncePerSec(cb: () => Unit) {
    while(true) {
      cb()
      Thread sleep 10
    }
  }

  def main(args: Array[String]) {
    oncePerSec(() => println("hello"))
  }
}
