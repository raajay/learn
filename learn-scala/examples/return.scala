class MyClass {
  def a : Int = {
    5 * 5
  }

  // the return type for b is inferred a int/string depending on what is the
  // last statement
  def b = {
    1
    "vini"
  }
}


object ReturnTest {
  def main(args: Array[String]) {
    var x = new MyClass
    println(x.a)
    println(x.b)
  }
}
