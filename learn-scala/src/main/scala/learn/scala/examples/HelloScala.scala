package learn.scala.examples
import scala.collection.JavaConversions._

/**
  * Author : Raajay Viswanathan
  * e-mail : raajay.v@gmail.com 
  * Date : 2/6/16
  */
object HelloScala {

  def loop() : Unit = {
    for(i <- 100 to 110) {
      println(i)
    }

    val mylist = List("1", 2)
    for(x <- mylist) {
      println(x)
    }
  }


  def javaInterface(): Unit = {
    val c = new Classroom()
    for(student <- c.getRollNumbers) {
      println(student)
    }
  }

  def main(args: Array[String]): Unit = {
    javaInterface()
  }

}
