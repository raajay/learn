package scala99

/**
  * Author : Raajay Viswanathan
  * e-mail : raajay.v@gmail.com 
  * Date : 9/10/16
  */
object p05 {
  def revList[A](ls: List[A]): List[A] = {
    ls match {
      case Nil => Nil
      case h :: tail => revList(tail) ::: List(h)
    }
  }


  def revListTail[A](ls: List[A]): List[A] = {

  }
}
