package scala99

/**
  * Author : Raajay Viswanathan
  * e-mail : raajay.v@gmail.com 
  * Date : 9/10/16
  */
object p03 {

  def findKthElemTrivial[A](k: Int, ls: List[A]): A = {
    if(k >= 0)
      ls(k)
    else
      throw new NoSuchElementException
  }

  def findKthElem[A](k: Int, ls: List[A]): A = {
    (k, ls) match {
      case (0, h :: _) => h
      case (a, _ :: tail) => findKthElem(a-1, tail)
      case(_, Nil) => throw new NoSuchElementException
    }
  }

}
