package scala99

/**
  * Author : Raajay Viswanathan
  * e-mail : raajay.v@gmail.com 
  * Date : 9/10/16
  */
object run {
  val a = List(1,1,2,3,5,8)

  def run01 (): Unit = {
    println(p01.lastBuiltin(a))
    println(p01.lastCustom(a))
  }

  def run02() : Unit = {
    println(p02.penultimate(a))
  }

  def run05() : Unit = {
    println(p05.revList(a))
  }

  def main(args: Array[String]): Unit = {
//    run01()
//    run02()
        run05()
  }
}
