package scala99

/**
  * Author : Raajay Viswanathan
  * e-mail : raajay.v@gmail.com 
  * Date : 9/10/16
  */
object p01 {

  def lastBuiltin[A](ls: List[A]): A = {
    ls.last
  }

  def lastCustom[A](ls: List[A]): A = {
    ls match {
      case h :: Nil => h
      case _ :: tail => lastCustom(ls.tail)
      case _ => throw new NoSuchElementException
    }
  }

}
