package scala99

/**
  * Author : Raajay Viswanathan
  * e-mail : raajay.v@gmail.com 
  * Date : 9/10/16
  */
object p02 {

  def penultimate[A](ls: List[A]) : A = {
    ls match {
      case x :: y :: Nil => x
      case x :: tail => penultimate(tail)
      case _ => throw new NoSuchElementException
    }
  }

}
