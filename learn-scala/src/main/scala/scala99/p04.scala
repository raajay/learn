package scala99

/**
  * Author : Raajay Viswanathan
  * e-mail : raajay.v@gmail.com 
  * Date : 9/10/16
  */
object p04 {

  def numElemTrivial[A](ls: List[A]): Int = {
    ls.length
  }

  def numElem[A](ls: List[A]): Int = ls match {
    case Nil => 0
    case _ :: tail => 1 + numElem(tail)
  }

  def numElemTailRecurse[A](ls: List[A]): Int = {
    def lengthR(result:Int, curList:List[A]): Int = {
      curList match {
        case Nil => result
        case _ :: tail => lengthR(result + 1, tail)
      }
    }
    lengthR(0, ls)
  }

  def numElemFunc[A](ls: List[A]): Int = {
    ls.foldLeft(0)({(c, _) => c+1})
  }
}
