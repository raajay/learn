package learn.scala.examples;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Author : Raajay Viswanathan
 * e-mail : raajay.v@gmail.com
 * Date : 2/6/16
 */
public class Classroom {

  protected Map<Integer, String> _roll_call;

  public Classroom() {
    _roll_call = new HashMap<>();
    _roll_call.put(1, "Alice");
    _roll_call.put(2, "Bob");
    _roll_call.put(3, "Charlie");
  }

  public int getNumStudents() {
    return _roll_call.size();
  }

  public Set<Integer> getRollNumbers() {
    return _roll_call.keySet();
  }

  public Set<String> getName() {
    return new HashSet<>(_roll_call.values());
  }
}
