from bokeh.plotting import figure, HBox, output_file, show, VBox
from bokeh.models import Range1d
import os

x1 = [0,1,2,3,4,5,6,7,8,9,10]
y1 = [0,8,2,4,6,9,15,18,19,25,28]

output_file("scatter.html")

p1 = figure(plot_width=300, plot_height=300)
p1.line(x1, y1, size=12, color="red", alpha=0.5)

p2 = figure(plot_width=300, plot_height=300)
p2.bar(x1, [2*x for x in y1], line_dash="dashed", line_width=4, line_color="blue")



p3 = figure(plot_width=300, plot_height=300)

p4 = figure(plot_width=900, plot_height=300)

show(VBox(HBox(p1,p2,p3), p4))
