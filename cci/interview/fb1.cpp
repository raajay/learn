#ifndef FB_INTERVIEW_1
#define FB_INTERVIEW_1

#include "../4/mytree.hpp"
#include <iostream>
#include <cstdlib>



bool isLeaf(BinaryTreeNode* root) {
    return !root->left && !root->right;
}

BinaryTreeNode* treeToList(BinaryTreeNode* root) {
    if(!root) {
        return nullptr;
    }

    if(isLeaf(root)) {
        root->right = root;
        root->left = root;
        return root;
    }

    BinaryTreeNode* left_tree_list_head = treeToList(root->left);
    BinaryTreeNode* right_tree_list_head = treeToList(root->right);

    BinaryTreeNode* left_tree_list_tail = (left_tree_list_head) ? left_tree_list_head->left : nullptr; // prev
    BinaryTreeNode* right_tree_list_tail = (right_tree_list_head) ? right_tree_list_head->left : nullptr;

    if(left_tree_list_head && !right_tree_list_head) {
        left_tree_list_tail->right = root;
        root->left = left_tree_list_tail;
        root->right = left_tree_list_head;
        left_tree_list_head->left = root;
        return left_tree_list_head;
    } else if(right_tree_list_head && !left_tree_list_head) {
        root->right = right_tree_list_head;
        right_tree_list_head->left = root;
        right_tree_list_tail = root;
        root->left = right_tree_list_tail;
        return root;
    } else {
        left_tree_list_tail->right = root;
        root->left = left_tree_list_tail;
        root->right = right_tree_list_head;
        right_tree_list_head->left = root;
        left_tree_list_head->left = right_tree_list_tail;
        right_tree_list_tail->right = left_tree_list_head;
        return left_tree_list_head;
    }
}


void displayDoublyLinkedList(BinaryTreeNode* head) {
    BinaryTreeNode* iter = head;
    while(iter) {
        std::cout << iter->key << " ";
        iter = iter->right;
        if(iter == head) {
            break;
        }
    }
    std::cout << std::endl;
}


int main() {
    int preorder[] = {4, 2, 1, 3, 6, 5, 7};
    int inorder[] = {1,2,3,4,5,6,7};
    BinaryTreeNode* root = createTree(inorder, preorder, 7);
    displayDoublyLinkedList(treeToList(root));
    return 0;
}


#endif /* ifndef SYMBOL */
