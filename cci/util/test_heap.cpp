#include <algorithm>
#include <vector>
#include <iostream>

int main() {
    std::vector<int> myval {1,2,3,4,5,6,7,8};
    make_heap(myval.begin(), myval.end());
    for(auto val : myval) {
        std::cout << val << " ";
    }
    std::cout << std::endl;
    return 0;
}
