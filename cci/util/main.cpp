#include <iostream>
#include "../4/myheap.hpp"

int main() {
    TestMinHeap heap_tester;
    heap_tester.runTests();
    return 0;
}