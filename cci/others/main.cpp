#include<iostream>
#include<string>
using namespace std;

bool validParentheses(string s) {
    int nopen = 0;
    for(size_t i = 0; i < s.length() ; i++) {
        nopen += (s[i] == '(') ? 1 : -1;
        if(nopen < 0) {
            return 0;
        }
    }
    return nopen == 0;
}

int main() {
    std::cout << validParentheses("()") << std::endl;
    std::cout << validParentheses("(") << std::endl;
    std::cout << validParentheses("(((())))") << std::endl;
}
