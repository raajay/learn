#ifndef MY_GRAPH_H_
#define MY_GRAPH_H_

#include <vector>
#include <set>
#include <unordered_map>

class GraphNode {
	// value on each node
    int value;
    // an vector of neighbors 
    std::set<GraphNode*> neighbors;
public:
	GraphNode(int value) {
		this->value = value;
	}

	void addNeighbor(GraphNode* neighbor) {
		neighbors.insert(neighbor);
	}

	void removeNeighbor(GraphNode* neighbor) {
		neighbors.erase(neighbor);
	}

	void clearNeighborsInformation() {
		neighbors.clear();
	}

	std::set<GraphNode*> getNeighbors() {
		return neighbors; // does this make a copy? 
	}
};

class UndirectedGraph{
	// A list of nodes
	std::set<GraphNode*> nodes;
public:

	GraphNode* createAndAddNode(int value) {
		GraphNode* new_node = new GraphNode(value);
		this->addNode(new_node);
		return new_node;
	}

	void addNode(GraphNode* node) {
		nodes.insert(node); // edges are added separately
	}

	void deleteNode(GraphNode* node) {
		// delete a node and the edges attached to it
		nodes.erase(node);
		// remove this entry from all the neighbors
		for(GraphNode* neighbor : node->getNeighbors()) {
			neighbor->removeNeighbor(node);
		}
		// remove neighbors from my neighbor list
		node->clearNeighborsInformation();

		// should we clean up current node?
	}

	void addEdge(GraphNode* node_1, GraphNode* node_2) {
		node_1->addNeighbor(node_2);
		node_2->addNeighbor(node_1);
	}

	void removeEdge(GraphNode* node_1, GraphNode* node_2) {
		node_1->removeNeighbor(node_2);
		node_2->removeNeighbor(node_1);
	}

};


UndirectedGraph* createUndirectedGraph(std::vector<std::pair<int, int>> edges) {
	std::unordered_map<int, GraphNode*> nodes;
	UndirectedGraph* graph = new UndirectedGraph();
	for(std::pair<int, int> edge : edges) {
		if(nodes.find(edge.first) == nodes.end()) {
			nodes[edge.first] = graph->createAndAddNode(edge.first);
		}
		if(nodes.find(edge.second) == nodes.end()) {
			nodes[edge.second] = graph->createAndAddNode(edge.second);
		}
		graph->addEdge(nodes[edge.first], nodes[edge.second]);
	}
	return graph;
}

#endif