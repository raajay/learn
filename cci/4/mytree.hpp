#ifndef MY_TREE_H_
#define MY_TREE_H_

#include <vector>
#include <iostream>

class BinaryTreeNode {
public:
    BinaryTreeNode(int value) : key(value), left(nullptr), right(nullptr) {}
    int key;
    BinaryTreeNode* left;
    BinaryTreeNode* right;
};


BinaryTreeNode* createTree(int *inorder, int *preorder, int n) {
    if(n == 0) return nullptr;
    int root_value = preorder[0];
    int root_idx = -1;
    for(int i = 0; i < n; i++) {
        if(inorder[i] == root_value) {
            root_idx = i;
            break;
        }
    }
    BinaryTreeNode* root = new BinaryTreeNode(root_value);
    root->left = createTree(inorder, &(preorder[1]), root_idx);
    root->right = createTree(&inorder[1 + root_idx], &preorder[1 + root_idx], n - 1 - root_idx);
    return root;
}

void deleteTree(BinaryTreeNode* root) {
    if(!root) return;
    deleteTree(root->left);
    deleteTree(root->right);
    delete root;
}


void displayInorder(BinaryTreeNode *root) {
    if(!root)
        return;
    displayInorder(root->left);
    std::cout << root->key << " ";
    displayInorder(root->right);
}

void displayPostOrder(BinaryTreeNode *root) {
    if(!root) return;
    displayPostOrder(root->left);
    displayPostOrder(root->right);
    std::cout << root->key << " ";
}

bool checkIdenticalBinaryTree(BinaryTreeNode* tree1, BinaryTreeNode* tree2) {
    if(!tree1 && !tree2)
        return true; // identical if both are none
    if((!tree1 && tree2) || (tree1 && !tree2)) {
        return false; // if exactly one is null
    }
    return (tree1->key == tree2->key)
            && checkIdenticalBinaryTree(tree1->left, tree2->left)
            && checkIdenticalBinaryTree(tree1->right, tree2->right);
}


class BinaryTreeTester {
public:
    void runTests() {
        int preorder[] = {4, 2, 1, 3, 6, 5, 7};
        int inorder[] = {1,2,3,4,5,6,7};
        BinaryTreeNode* root = createTree(inorder, preorder, 7);
        displayInorder(root); std::cout << std::endl;
        displayPostOrder(root); std::cout << std::endl;
    }

};

#endif
