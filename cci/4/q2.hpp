#ifndef CH4_QUESTION_2_H
#define CH4_QUESTION_2_H

#include "../problem.hpp"
#include "mytree.hpp"

class Ch4Question2 : public Problem {

	BinaryTreeNode* createMinHeightSearchTree(int *values, int n) {
		if (n == 0) return nullptr;
		int middle_index = n /2 ;
		BinaryTreeNode *root = new BinaryTreeNode(values[middle_index]);
		// create left tree
		root->left = createMinHeightSearchTree(values, middle_index);
		// create right tree
		root->right = createMinHeightSearchTree(values + middle_index + 1, n - 1 - middle_index);
		return root;
	}


    int check(int *inorder, int *preorder, int n) {
        BinaryTreeNode *solution = createMinHeightSearchTree(inorder, n);
        BinaryTreeNode *expected = createTree(inorder, preorder, n);
        int retval = checkIdenticalBinaryTree(solution, expected) ? 0 : 1;
        // displayPostOrder(solution); std::cout << std::endl;
        // displayPostOrder(expected); std::cout << std::endl;
        deleteTree(solution);
        deleteTree(expected);
        return retval;
    }

    void defineTests() override {
    	int inorder[] = {1,2,3,4,5,6,7};
    	int preorder[] = {4, 2, 1, 3, 6, 5, 7};
        errors += check(inorder, preorder, 7);
    }

public:
	Ch4Question2() {
		chapterNum = 4;
		questionNum = 2;
	}
};

#endif