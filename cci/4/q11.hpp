#ifndef CH_4_QUESTION_11_H
#define CH_4_QUESTION_11_H

#include "mytree.hpp"
#include "../problem.hpp"
#include <iostream>
#include <algorithm>
#include <vector>
#include <cstdlib>


class TreeNodeWithCount : public BinaryTreeNode {
public:
    TreeNodeWithCount(int x) : BinaryTreeNode(x), count_(1) {}
    int count_;

    bool isLeafNode() {
        return this->left || this->right;
    }
};
typedef TreeNodeWithCount SpecialNode;
typedef BinaryTreeNode Node;

Node* addNodeIterative(Node *root, int x) {
    if(!root) {
        // create a new node and return a pointer to it
        Node * new_node = new SpecialNode(x);
        return new_node;
    }

    Node* iter = root;
    while(1) {
        ((SpecialNode*) iter)->count_++;
        if(x > iter->key) {
            if(iter->right == nullptr) {
                iter->right = new SpecialNode(x);
                break;
            } else {
                iter = iter->right;
            }
        } else {
            if(iter->left == nullptr) {
                iter->left = new SpecialNode(x);
                break;
            } else {
                iter = iter->left;
            }
        }
    }

    return root;
}

Node * addNode (Node* root, int x) {
    if(!root) {
        // create a new node and return a pointer to it
        Node * new_node = new SpecialNode(x);
        return new_node;
    }
    if(x > root->key) {
        root->right = addNode(root->right, x);
    } else {
        root->left = addNode(root->left, x);
    }
    ((SpecialNode *)root)->count_++;
    return root;
}


Node* minNode(Node* root) {
    if(!root) return nullptr;
    while(root->left) {
        root = root->left;
    }
    return root;
}

Node* deleteNode (Node* root, int x, Node* &deletedNode) {
    if(!root) {
        deletedNode = nullptr;
        return nullptr;
    }

    int count_ = ((SpecialNode*)root)->count_;

    if(x > root->key) {
        root->right = deleteNode(root->right, x, deletedNode);
        ((SpecialNode*)root)->count_ = count_ - 1;
        return root;
    } else if(x < root->key) {
        root->left = deleteNode(root->left, x, deletedNode);
        ((SpecialNode*)root)->count_ = count_ - 1;
        return root;
    } else {
        deletedNode = root;
        if(root->right == nullptr) {
            return root->left;
        } else {
            Node* min_right = minNode(root->right);
            Node* deleted_node;
            Node *new_right_branch = deleteNode(root->right, min_right->key, deleted_node);
            if(min_right != deleted_node) {
                std::cout << "Deleted node is not the min node on the right" << std::endl;
            }
            min_right->left = root->left;
            min_right->right = new_right_branch;
            ((SpecialNode*)min_right)->count_ = count_ - 1;
            return min_right;
        }
    }
}


Node* findNode (Node* root, int x) {
    if(!root) return nullptr;
    if(x == root->key) {
        return root;
    } else if (x > root->key) {
        return findNode(root->right, x);
    } else {
        return findNode(root->left, x);
    }
}

Node* findNodeIterative(Node* root, int x) {
    if(!root) return nullptr;
    while(root) {
        if(x == root->key) {
            return root;
        } else if (x > root->key) {
            root = root->right;
        } else {
            root = root->left;
        }
    }
    return nullptr;
}

Node* getRandomNode(Node* root, int index=-1) {
    if(!root)
        return nullptr;

    if(index < 0)
        index = 1 + rand() % ((SpecialNode*) root)->count_;

    int left_tree_size = (root->left) ? ((SpecialNode*)root->left)->count_ : 0;
    int right_tree_size = (root->right) ? ((SpecialNode*)root->right)->count_ : 0;
    if(index <= left_tree_size) {
        return getRandomNode(root->left, index);
    } else if(index == left_tree_size + 1) {
        return root;
    } else {
        return getRandomNode(root->right, index - left_tree_size -1);
    }
}


class Ch4Question11 :  public Problem {

    int check() {
        int NUM_NODES = 100;
        std::vector<int> values;
        for(int i = 0; i < NUM_NODES; i++) {
            values.push_back(i);
        }
        std::random_shuffle(values.begin(), values.end());
        // create a tree
        Node *root = nullptr;
        for(int i = 0; i < NUM_NODES; i++) {
//            root = addNode(root, values[i]);
            root = addNodeIterative(root, values[i]);
        }
        std::cout << "Finished adding nodes" << std::endl;
        for(int i = 0; i < 10; i++) {
            Node* dummy;
            root = deleteNode(root, values[i], dummy);
        }
        // let us get many random calls and see what the histogram looks like
        int histogram[NUM_NODES];
        memset(histogram, 0, NUM_NODES * sizeof(int));

        int NUM_CALLS = 1000000;
        for(int i = 0; i < NUM_CALLS; i++) {
            Node *sample = getRandomNode(root);
            histogram[sample->key]++;
        }
        std::cout << "max = " << *(std::max_element(histogram, histogram + NUM_NODES)) << std::endl;
        std::cout << "min = " << *(std::min_element(histogram, histogram + NUM_NODES)) << std::endl;
        int zero_count = 0;
        for(int i = 0; i < NUM_NODES; i++) {
            if (histogram[i] == 0)
                zero_count++;
        }
        std::cout << "#zeros = " << zero_count << std::endl;
        return 0;
    }

    void defineTests() override {
        errors += check();
    }

public:
    Ch4Question11() : Problem() {
        chapterNum = 4;
        questionNum = 11;
    }
};


#endif
