#ifndef MY_HEAP_H_
#define MY_HEAP_H_

#include <iostream>
#include <cstring>
#include <cstdlib>

class MinHeap {
    static const long MAX_SIZE = 1000000L;
public:
    MinHeap() {
        size_ = 0;
        data = new int[MAX_SIZE];
        memset(data, 0, MAX_SIZE*sizeof(int));
        std::cout << "Created the data structures for MinHeap" << std::endl;
    }

    void add(int value) {
        size_++;
        data[size_] = value;
        int childIndex = size_; 
        while(childIndex > 1) {
            int parentIndex = childIndex / 2;
            if(data[childIndex] < data[parentIndex]) {
                // swap with parent if child is smaller than parent
                int tmp = data[childIndex];
                data[childIndex] = data[parentIndex];
                data[parentIndex] = tmp;
                // update child
                childIndex /= 2;

            } else {
                break;
            }
        }
    }

    int remove_min() {
        int retval = data[1];
        data[1] = data[size_];
        size_--;
        // we have to reset the heap property again.
        int currIndex = 1;
        while(2 *  currIndex <= size_) {
            int smallChildIndex;
            if (2 * currIndex == size_) {
                smallChildIndex = 2 * currIndex;
            } else {
                smallChildIndex = (data[2 * currIndex] < data[2 * currIndex + 1]) ? 2 * currIndex : 2 * currIndex + 1;
            }

            if(data[smallChildIndex] < data[currIndex]) {
                // swap
                int tmp  = data[currIndex];
                data[currIndex] = data[smallChildIndex];
                data[smallChildIndex] = tmp;

                currIndex = smallChildIndex;
            } else {
                break;
            }
        }
        return retval;
    }

    int peek() {
        return data[1];
    }

    long size() {
        return size_;
    }

private:
    long size_;
    int *data;
};


class TestMinHeap {

public:
    void runTests() {
        long N = 100000L;
        MinHeap heap;
        while(N--) {
            heap.add(rand() % 1000);
        }
        std::cout << "Add elements successful" << std::endl;
        long iters = 10L;
        long prev = -1;
        while(iters--) {
            int min_val = heap.remove_min();
            if(prev > min_val) {
                std::cout << "Error in MinHeap implementation" << std::endl; 
                std::cout << "Prev = " << prev << " Min = " << min_val << std::endl;
            }
            prev = min_val;
        }

    }
};


#endif