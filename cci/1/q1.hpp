#ifndef CHAP_1_Q1_H
#define CHAP_1_Q1_H

#include <algorithm>
#include <bitset>
#include <iostream>
#include <cstring>
#include <unordered_map>

#include "problem.hpp"

class Ch1Question1 : public Problem {

    bool hasUniqueCharacters(std::string s) {
        /* 1. Use hash table to track characters already seen.
            2. Iterate through all characters. If already seen, return false.
            3. End of loop return true
        */
        std::unordered_map<char, bool> seen_chars;
        for(int i = 0; i < s.length(); i++) {
            char c = s[i];
            if(seen_chars.find(c) != seen_chars.end()) {
                return false;
            }
            seen_chars[c] = true;
        }
        return true;
    }

    bool hasUniqueCharacters2(std::string s) {
        std::sort(s.begin(), s.end());
        for(int i = 1; i < s.length(); i++) {
            if(s[i-1] == s[i]) {
                return false;
            }
        }
        return true;
    }

    bool hasUniqueCharacters3(std::string s) {
        std::bitset<128> seen_chars; // create a 128 char bit set
        for(int i = 0; i < s.length(); i++) {
            char c = s[i];
            if(seen_chars[c]) {
                return false;
            } else {
                seen_chars.set(c);  
            }
        }
        return true;
    }

    int check(std::string input, bool expected) {
        bool answer = hasUniqueCharacters3(input);
        if (expected != answer) {
            std::cout << "Test Failed! Input: " << input << std::endl;
            std::cout << "Correct: " << (expected ? "True" : "False")
                << " Your answer: " << (answer ? "True" : "False" ) << std::endl;
            return 1;
        } else {
            return 0;
        }
    }

    void defineTests() override {
        errors += check("123weryhbalkjadfasdf", false);
        errors += check("1234567890", true);
        errors += check("123", true);        
    }

public:
    Ch1Question1() {
        this->chapterNum = 1;
        this->questionNum = 1;
    }
};


#endif