#ifndef CHAP_1_Q2_H
#define CHAP_1_Q2_H

#include <iostream>
#include <cstring>
#include "../problem.hpp"


class Ch1Question2 : public Problem {

    bool isPermutation(std::string s1, std::string s2) {
        sort(s1.begin(), s1.end()); // O(n log n)
        sort(s2.begin(), s2.end()); // O(n log n)
        for (size_t i = 0; i < s1.length(); i++) {
            if(s1[i] != s2[i])
                return false;
        }
        return true;
    }

    bool isPermutation2(std::string s1, std::string s2) {
        if(s1.length() != s2.length()) { // O (1)
            return false; // not a permutation if lengths are different
        }
        int count1[128], count2[128];
        memset(count1, 0, 128*sizeof(int));
        memset(count2, 0, 128*sizeof(int));

        for(size_t i = 0; i < s1.length(); i++) {
            char c = s1[i];
            count1[(int)c] += 1;
        }

        for(size_t i = 0; i < s2.length(); i++) {
            char c = s2[i];
            count2[(int)c] += 1;
        }

        for(int i = 0; i < 128; i++) {
            if(count1[i] != count2[i]) {
                return false;
            }
        }

        return true;
    }

    int check(std::string s1, std::string s2, bool expected) {
        bool answer = isPermutation2(s1, s2);
        if(answer != expected) {
            return 1;
        } else {
            return 0;
        }
    }

    void defineTests() override {
        errors += check("dog", "god", true);
    }
public:
    Ch1Question2() {
        chapterNum = 1;
        questionNum = 2;
    }
};

#endif
