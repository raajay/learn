#include <iostream>
#include "problem.hpp"
#include "1/q1.hpp"
#include "1/q2.hpp"
#include "2/q6.hpp"
#include "2/q1.hpp"
#include "4/q2.hpp"
#include "4/q11.hpp"

int main() {
    Problem *c1q1 = new Ch1Question1(); c1q1->runTests();
    Problem *c1q2 = new Ch1Question2(); c1q2->runTests();

    Problem *c2q6 = new Ch2Question6(); c2q6->runTests();
    Problem *c2q1 = new Ch2Question1(); c2q1->runTests();

    Problem *c4q2 = new Ch4Question2(); c4q2->runTests();
    Problem *c4q11 = new Ch4Question11(); c4q11->runTests();
    return 0;
}
