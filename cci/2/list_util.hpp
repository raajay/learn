#ifndef CCI_LIST_UTIL_H_
#define CCI_LIST_UTIL_H_

#include <vector>
#include <iostream>
#include <string>


class ListNode {
    public:
        ListNode(int x): key(x), next(nullptr) {

        }
        int key;
        ListNode* next;
};

ListNode* createList(std::vector<int> values) {
    ListNode head(0);

    int n = values.size();
    for(int i = 0; i < n ; i++) {
        // insert in reverse order since insert is at the head
        ListNode* new_node = new ListNode(values[n - i - 1]);
        new_node->next = head.next;
        head.next = new_node;
    }
    return head.next;
}

void deleteList(ListNode* &head) {
    ListNode* iter = head;
    while(iter) {
        head = iter->next;
        delete iter;
        iter = head;
    }
    head = nullptr;
}

void displayList(ListNode* head) {
    ListNode* iter = head;
    while(iter != nullptr) {
        std::cout << iter->key << ",";
        iter = iter->next;
    }
    std::cout << std::endl;
}

void displayVec(std::vector<int> values) {
    for(int i = 0; i < values.size(); i++) {
        std::cout << values[i] << " ";
    }
    std::cout << std::endl;
}

bool helperListEqualCornerCase(ListNode* l1, ListNode* l2) {
    if(l1 == nullptr && l2 == nullptr)
        return true;
    if((l1 != nullptr && l2 == nullptr) || (l1 == nullptr && l2 != nullptr))
        return false;
    std::cout << "Error: At least one of the two pointers have to be null" << std::endl;
    exit(-1);
}

bool areListsEqual(ListNode* l1, ListNode* l2) {
    if(!l1 || !l2) 
        return helperListEqualCornerCase(l1, l2);
    while(l1 != nullptr && l2 != nullptr) {
        if(l1->key != l2->key) {
            return false;
        }
        l1 = l1->next;
        l2 = l2->next;
    }
    return helperListEqualCornerCase(l1, l2);
}

std::string stringifyBool(bool value) {
    return value ? "True" : "False";
}

#endif