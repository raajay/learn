#ifndef CHAP_2_Q6_H
#define CHAP_2_Q6_H

#include "../problem.hpp"
#include "list_util.hpp"
#include <stack>
#include <iostream>

class Ch2Question6 : public Problem {

    bool isPalindrome(ListNode* head) {

        if(!head) {
            return true;
        }

        std::stack<ListNode*> s;
        ListNode* iter = head;
        while(iter) {
            s.push(iter);
            iter = iter->next;
        }

        iter = head;

        while(iter) {
            ListNode* top = s.top(); s.pop();
            if(top->key != iter->key) {
                return false;
            }
            if(iter->next == top || iter == top) {
                break;
            }
            iter = iter->next;
        }
        return true;
    }

    int check(std::vector<int> values, bool expected) {
        ListNode *head = createList(values);
        bool answer = isPalindrome(head);
        deleteList(head);
        int retval = (answer != expected) ? 1 : 0;
        if(retval) {
            std::cout << "Input: " << std::endl;
            displayVec(values);
            std::cout << "Your answer: " << stringifyBool(answer) << std::endl;
            std::cout << "Expected: " << stringifyBool(expected) << std::endl;
        }
        return retval;
    }

    void defineTests() override {
        std::vector<int> a1 = {1,2,3,4,5};
        errors += check(a1, false);

        std::vector<int> a2 = {1,2,3,2,1};
        errors += check(a2, true);

        std::vector<int> a3 = {1,2,2,1};
        errors += check(a3, true);
    }


public:
    Ch2Question6() {
        chapterNum = 2;
        questionNum = 6;
    }
};

#endif