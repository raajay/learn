#ifndef CHAP_2_Q1_H
#define CHAP_2_Q1_H

#include "../problem.hpp"
#include "list_util.hpp"
#include <stack>
#include <iostream>
#include <unordered_map>

class Ch2Question1 : public Problem {

    ListNode* removeDuplicatesUnsorted(ListNode* head) {
        ListNode dummy(0); 
        dummy.next = head;
        ListNode *prev = &dummy;

        std::unordered_map<int, bool> memory;
        ListNode *iter = head;
        while(iter) {
            if(memory.find(iter->key) != memory.end()) {
                // if key is found, delete the node
                prev->next = iter->next;
                delete iter;
                iter = prev->next;
            } else {
                memory[iter->key] = true;
                prev = iter;
                iter = iter->next;
            }
        }
        return dummy.next;
    }

    int check(std::vector<int> input, std::vector<int> expected) {
        ListNode *head = createList(input);
        ListNode *head2 = createList(expected);
        ListNode* answer = removeDuplicatesUnsorted(head);

        int retval = (!areListsEqual(answer, head2)) ? 1 : 0;

        if(retval) {
            std::cout << "Input: "; displayVec(input);
            std::cout << "Your answer: "; displayList(answer);
            std::cout << "Expected: "; displayList(head2) ;
        }

        deleteList(answer);
        deleteList(head2);
        return retval;
    }

    void defineTests() override {
        std::vector<int> a1 = {1,2,3,4,5};
        std::vector<int> b1 = {1,2,3,4,5};
        errors += check(a1, b1);

        std::vector<int> a2 = {1,2,3,2,1};
        std::vector<int> b2 = {1,2,3};
        errors += check(a2, b2);

        std::vector<int> a3 = {1,2,2,1};
        std::vector<int> b3 = {1,2};
        errors += check(a3, b3);

        std::vector<int> a4;
        errors += check(a4,a4);
    }


public:
    Ch2Question1() {
        chapterNum = 2;
        questionNum = 1;
    }
};

#endif