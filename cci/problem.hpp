#ifndef PROBLEM_H_
#define PROBLEM_H_

#include <iostream>


class Problem {
public:
    void runTests() {
        std::cout << "Running tests for Chapter " << chapterNum
             << " : Question " << questionNum << std::endl;
        errors = 0;
        defineTests();
        if(errors) {
            std::cout  << errors << " test case(s) failed." << std::endl;
        } else {
            std::cout << "All tests passed." << std::endl;
        }
    }

protected:
    int errors;
    int chapterNum;
    int questionNum;
    virtual void defineTests() {}
};
#endif