#include <iostream>

class BTNode {
 public:
  int key;
  BTNode *left;
  BTNode *right;

  BTNode(int key1) {
    key = key1;
    left = nullptr;
    right = nullptr;
  }
};

class Graph {
 public:
};
