//
// Created by Raajay on 10/18/16.
//

#include "MySocket.h"

Socket :: Socket() {
    m_sock = -1;
    memset(&m_addr, 0, sizeof(m_addr));
}

Socket ::~Socket() {
    if(is_valid())
        ::close(m_sock);
}

bool Socket::create() {
    m_sock = socket(AF_INET, SOCK_STREAM, 0);

    if(!is_valid()) {
        return false;
    }

    return true;
}
