//
// Created by Raajay on 10/18/16.
//

#ifndef LEARN_CPP_MYSOCKET_H
#define LEARN_CPP_MYSOCKET_H

#include <string>
#include <netinet/in.h>

class Socket {
public:
    Socket();
    virtual ~Socket();

    // server initialization
    bool create();
    bool bind(const int port);
    bool listen() const;
    bool accept(Socket& )  const;


    // client initialization
    bool connect(const std::string host, const int port);

    // data transmission
    bool send(const std::string) const;
    int recv(std::string&) const;

    void set_non_blocking(const bool);
    bool is_valid() const {
        return m_sock != -1;
    }

private:
    int m_sock;
    sockaddr_in m_addr;

};

#endif //LEARN_CPP_MYSOCKET_H
