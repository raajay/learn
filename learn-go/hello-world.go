package main

import (
	"fmt"
	"math"
	"os"
)

func plus(a int, b int) int {
	return a + b
}

func factorial(n int) int {
	if n == 1 {
		return 1
	} else {
		return n * factorial(n-1)
	}
}

func testGoRoutine() {
	// create channel that accepts integers
	messages := make(chan int)

	i := 1
	for i <= 10 {
		// start go routines. Each sends messages to channel
		go func(val int) {
			messages <- val
		}(i)
		i++
	}

	i = 1
	for i <= 10 {
		msg := <-messages
		fmt.Println(msg)
		i++
	}
}

func testPointers() {
	a := 10
	pa := &a
	fmt.Println(pa)
	(*pa)++
	fmt.Println(a)
}

func main() {

	argsWithProg := os.Args
	argsWithoutProg := os.Args[1:]
	fmt.Println(argsWithoutProg)
	fmt.Println(argsWithProg)

	fmt.Println("hello world")
	fmt.Println("Raajay here")
	fmt.Println(1 + 2 + 3)
	name := "vini"
	fmt.Println(name)
	i := 1
	fmt.Println(i)
	i = i + 1
	fmt.Println(i)
	var name2 string = "raajay"
	fmt.Println(name2)

	const n = 500
	fmt.Println(math.Sin(n))

	for i < 10 {
		fmt.Println(i)
		i = i + 1
	}

	fmt.Println(plus(100, 200))
	fmt.Println(factorial(10))
	testPointers()
	testGoRoutine()
}
