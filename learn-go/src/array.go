package main

import "fmt"

func foo(a int) int {
	return a + 5
}

func main() {
	var a int
	a = 20
	a = foo(a)
	fmt.Println(a)
}
