package main

import (
	"bufio"
	"fmt"
	"net"
)

func main() {
	p := make([]byte, 2048)
	conn, err := net.Dial("udp", "127.0.0.1:1234")
	if err != nil {
		fmt.Printf("Error: %v", err)
		return
	}
	for {
		fmt.Fprintf(conn, "Hi server, how are you doing?")
		_, err = bufio.NewReader(conn).Read(p)
		if err == nil {
			fmt.Printf("The server says --> %s\n", p)
		} else {
			fmt.Printf("Some error %v\n", err)
		}
	}
	conn.Close()
}
