import gurobi.*;

public class mip1 {
  public static void main(String[] args) {
    try {
      GRBEnv env = new GRBEnv("mip1.log");
      GRBModel model = new GRBModel(env);

      // add variables
      GRBVar x = model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "x");
      GRBVar y = model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "y");
      GRBVar z = model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "z");

      model.update();

      GRBLinExpr expr = new GRBLinExpr();
      expr.addTerm(1.0, x);
      expr.addTerm(1.0, y);
      expr.addTerm(2.0, z);

      model.setObjective(expr, GRB.MAXIMIZE);

      // Add constraints
      expr = new GRBLinExpr();
      expr.addTerm(1.0, x);
      expr.addTerm(2.0, y);
      expr.addTerm(3.0, z);
      model.addConstr(expr, GRB.LESS_EQUAL, 4.0, "c0");

      expr = new GRBLinExpr();
      expr.addTerm(1.0, x);
      expr.addTerm(1.0, y);
      model.addConstr(expr, GRB.GREATER_EQUAL, 1.0, "c1");

      model.optimize();

      System.out.println(x.get(GRB.StringAttr.VarName) + " " + x.get(GRB.DoubleAttr.X));
      System.out.println(y.get(GRB.StringAttr.VarName) + " " + y.get(GRB.DoubleAttr.X));
      System.out.println(z.get(GRB.StringAttr.VarName) + " " + z.get(GRB.DoubleAttr.X));

      System.out.println("Obj: " + model.get(GRB.DoubleAttr.ObjVal));

      model.dispose();
      env.dispose();

    } catch (GRBException e) {
      System.out.println("Error code:" + e.getErrorCode() + "." + e.getMessage());
    }

  }

}
