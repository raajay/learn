#include "../util/util_structs.h"

class Solution {
public:
    ListNode* partition(ListNode* head, int x) {
    	if(head == nullptr)
    		return head;


        ListNode *left = nullptr;
        ListNode *left_tail = nullptr;
        ListNode *prev = nullptr;
    	ListNode* iter = head;

    	while(iter != nullptr) {


    		if(iter->val < x) {
    			// had to removed from this list
    			if(iter == head) {
    				// first element
    				head = iter->next;
    				if(left_tail == nullptr && left == nullptr) {
    					left = iter;
    					left_tail  = iter;
    				} else {
    					if(left_tail->next != nullptr)  {
    						std::cout << "ERROR";
    						// system.exit(-1);
    					}
    					left_tail->next = iter;
    					left_tail = iter;
    				}
    				iter->next = nullptr;
    				iter = head;
    			} else {

    				prev->next = iter->next;
    				iter->next = nullptr;

    				if(left_tail == nullptr && left == nullptr) {
    					// first element in new list
    					left = iter;
    					left_tail = iter;
    				} else {
    					left_tail->next = iter;
    					left_tail = iter;
    				}
    				
    				iter = prev->next;

    			}

    		} else { // skip this element
    			prev = iter;
    			iter = iter->next;
    		}

    	} // end while
    	//
    	if(left == nullptr) {
    		// all elemets are greater than or equal to x
    		return head;
    	} else {
    		left_tail->next = head;
    		return left;
    	}



    }
};