#include <iostream>
#include "solution.h"
#include <vector>

int main() {
    std::vector<int> a = {1,4,3,2,5,2};
    int n = a.size();
    std::cout << n << std::endl;

    ListNode* head = nullptr;
    for(int i = 0; i < n; i++) {
        ListNode* new_node = new ListNode(a[n-i-1]);
        new_node->next = head;
        head = new_node;
    }
    display(head);
    Solution s;
    display(s.partition(head, 3));
}
