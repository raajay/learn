//
// Created by raajay on 1/9/17.
//

#include "Solution.h"

int main() {
    std::vector<int> a = {1,2,3,4,5};
    ListNode* l1 = createLinkedList(a);
    display(l1);
    Solution s;
//    ListNode* r1 = s.reverseBetween(l1, 2,4);
//    display(r1);

    ListNode* r2 = s.reverseBetween(createLinkedList(a), 2,4);
    display(r2);

    return 0;
}
