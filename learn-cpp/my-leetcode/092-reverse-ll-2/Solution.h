//
// Created by raajay on 1/9/17.
//

#ifndef LEARN_CPP_SOLUTION_H
#define LEARN_CPP_SOLUTION_H

#include "../util/util_structs.h"

class Solution {
    int n = 0, m = 0;

    void recurseRevBetween(ListNode* curr, ListNode* &head, ListNode* &tail, int idx) {
        ListNode* new_head(nullptr);
        ListNode* new_tail(nullptr);
        if(idx < m) {
            recurseRevBetween(curr->next, new_head, new_tail, idx+1);
            curr->next = new_head;
            head = curr;
        } else if(idx >= m && idx < n) {
            recurseRevBetween(curr->next, new_head, new_tail, idx+1);
            head = new_head;
            curr->next = new_tail->next;
            new_tail->next = curr;
            tail = curr;
        } else {
            head = curr;
            tail = curr;
        }
    }

public:
    ListNode* reverseBetween(ListNode* head, int m, int n) {
        this->n = n;
        this->m = m;
        ListNode* new_head(head);
        ListNode* new_tail(nullptr);
        recurseRevBetween(head, new_head, new_tail, 1);
        return new_head;
    }
};
#endif //LEARN_CPP_SOLUTION_H
