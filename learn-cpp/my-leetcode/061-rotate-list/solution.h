#include "../util/util_structs.h"

class Solution {
public:
    ListNode* rotateRight(ListNode* head, int k) {
        int length = 0;
        ListNode* iter = head;
        ListNode* last = nullptr;
        while(iter != nullptr) {
            length++;
            last = iter;
            iter = iter->next;
        }
        if(length <= 1) {
            return head;
        }
        k = k % length;
        iter = head;
        for(int i = 0; i < length - k - 1; i++) {
            iter = iter->next;
        }
        last->next = head;
        head = iter->next;
        iter->next = nullptr;

        return head;
    }
};
