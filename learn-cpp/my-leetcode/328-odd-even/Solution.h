//
// Created by Raajay on 1/8/17.
//

#ifndef LEARN_CPP_SOLUTION_H
#define LEARN_CPP_SOLUTION_H

#include "../util/util_structs.h"

class Solution {
public:
    ListNode* oddEvenList(ListNode* head) {
        ListNode oddHead(0), evenHead(0);
        ListNode *l1 = &oddHead;
        ListNode *l2 = &evenHead;
        int i = 1;
        while(head) {
            if(i % 2 == 1) {
                l1->next = head;
                l1 = head;
            } else {
                l2 -> next = head;
                l2 = head;
            }
            i++;
            head = head->next;
        }
        // l1 would be prev of the odd list
        l1->next = evenHead.next;
        l2->next = nullptr;
        return oddHead.next;
    }
};

#endif //LEARN_CPP_SOLUTION_H
