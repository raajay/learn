//
// Created by Raajay on 1/8/17.
//

#include "Solution.h"

int main() {
    std::vector<int> a = {1,2,3,4,5};
    ListNode* l1 = createLinkedList(a);
    display(l1);
    Solution s;
    display(s.oddEvenList(l1));
    return 0;
}