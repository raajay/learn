//
// Created by Raajay on 1/8/17.
//

#ifndef LEARN_CPP_SOLUTION_H
#define LEARN_CPP_SOLUTION_H
#include "../util/util_structs.h"

class Solution {
public:
    ListNode* deleteDuplicates(ListNode* head) {
        ListNode h2(0);
        h2.next = head;
        ListNode* prev = &h2;
        bool remove_next = false;
        while(head) {
            bool remove_current = remove_next;
            remove_next = head->next != nullptr && head->val == head->next->val;
            if(remove_current || remove_next) {
                prev->next = head->next;
            } else {
                prev = head;
            }
            head = head->next;
        }
        return h2.next;
    }
};


#endif //LEARN_CPP_SOLUTION_H
