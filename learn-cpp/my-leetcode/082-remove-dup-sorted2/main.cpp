//
// Created by Raajay on 1/8/17.
//

#include "solution.h"

int main () {
    std::vector<int> a = {1,2,3,3,4,4,5};
    ListNode* l1 = createLinkedList(a);
    display(l1);

    Solution s;
    display(s.deleteDuplicates(l1));

    std::vector<int> b = {1,1,1,2,3};
    ListNode* l2 = createLinkedList(b);
    display(l2);
    display(s.deleteDuplicates(l2));

    return 0;
}