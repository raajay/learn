//
// Created by raajay on 1/9/17.
//

#include "Solution.h"

int main() {
    std::vector<int> a = {1,2,3,4,5};
    std::vector<int> b = {1,2,3,4};
    ListNode* l1 = createLinkedList(a);
    ListNode* l2 = createLinkedList(b);

    display(l1);
    Solution s;
    l1 = s.reverseList(l1);
    display(l1);

    display(l2);
    l2 = s.reverseList(l2);
    display(l2);

    return 0;
}
