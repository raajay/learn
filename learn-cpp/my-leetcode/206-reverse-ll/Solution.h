//
// Created by raajay on 1/9/17.
//

#ifndef LEARN_CPP_SOLUTION_H
#define LEARN_CPP_SOLUTION_H
#include "../util/util_structs.h"

class Solution {

    ListNode* recursiveReverse(ListNode *currhead, ListNode* &head) {
        if(!currhead) {
            return nullptr;
        }
        ListNode* tail = recursiveReverse(currhead->next, head);
        if(!tail) {
            head = currhead;
        } else {
            tail->next =currhead;
        }
        currhead->next = nullptr;
        return currhead;
    }

public:
    ListNode* reverseList(ListNode* head) {
        if(!head)
            return head;
        ListNode* new_head;
        recursiveReverse(head, new_head);
        return new_head;
    }
};

#endif //LEARN_CPP_SOLUTION_H
