//
// Created by Raajay Viswanathan
//
#ifndef LEETCODE_SOLUTION_338_H
#define LEETCODE_SOLUTION_338_H
#include <vector>
using namespace std;

class Solution338 {
public:
    vector<int> solve(int num) {
        vector<int> retval;
        retval.push_back(0);
        if(num == 0)
            return retval;
        retval.push_back(1);

        unsigned int shift = 1;
        for (int i = 2; i <= num; i++) {
            int a = 1 << shift; // 2
            retval.push_back(1 + retval[i - a]);
            if(i == a + a-1) {
                shift++;
            }
        }
        return retval;
    }
};

#endif
