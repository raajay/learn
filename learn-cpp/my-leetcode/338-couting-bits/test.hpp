//
// Created by Raajay Viswanathan
//
#ifndef LEETCODE_TEST_SOLUTION_338_H
#define LEETCODE_TEST_SOLUTION_338_H

#include <iostream>
#include <vector>
#include "solution.hpp"
#include "../util/funcs.hpp"

class TestSolution338 {

    int check(int num, std::vector<int> expected) {
        Solution338 s;
        std::vector<int> answer = s.solve(num);
        int retval = 0;
        for(size_t i = 0; i < expected.size(); i++) {
            if(answer[i] != expected[i]) {
                retval = 1;
                break;
            }
        }
//        if(retval == 1) {
            std::cout << "Answer = ";
            displayVec(answer);
            std::cout << "Expected = ";
            displayVec(expected);
//        }
        return 0;
    }

public:
    void runTests() {
        int errors = 0;

        int a[] = {0,1,1,2,1,2};
        std::vector<int> expected(a, a+6);
        errors += check(5, expected);

        // fill in the test cases
        if(errors == 0) {
            std::cout << "All tests passed." << std::endl;
        }
    }
};

#endif
