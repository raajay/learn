//
// Created by Raajay on 1/7/17.
//

#include <iostream>
#include "solution.h"
#include <vector>

int main() {
    std::vector<int> a = {7,2,4,3};
    std::vector<int> b = {5,6,4};

    ListNode* l1 = createLinkedList(a);
    ListNode* l2 = createLinkedList(b);

    //
    display(l1);
    display(l2);

    Solution s;
    display(s.addTwoNumbers(l1, l2));
}
