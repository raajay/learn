//
// Created by Raajay on 1/7/17.
//

#ifndef LEARN_CPP_SOLUTION_H
#define LEARN_CPP_SOLUTION_H

#include "../util/util_structs.h"

class Solution {
protected:
    /*
     * Returns the tail of the reversed lists starting with next node
     */
    ListNode* recursiveWorker(ListNode* currhead, ListNode* &head) {

        // last element of the list
        if(currhead->next == nullptr) {
            head = currhead;
        } else {
            ListNode* tail = recursiveWorker(currhead->next, head);
            tail->next = currhead;
            currhead->next = nullptr;
        }
        return currhead;
    }

    ListNode* recursiveReverseLL(ListNode* head) {
        ListNode* newhead(nullptr);
        recursiveWorker(head, newhead);
        return newhead;
    }

    ListNode* iterativeReverseLL(ListNode* head) {
        ListNode *prev(nullptr), *curr(head);
        while(curr) {
            ListNode *nexthop(curr->next);
            curr->next = prev;
            prev = curr;
            curr = nexthop;
        }
        return prev;
    }

    ListNode* addTwoReverse(ListNode* l1, ListNode* l2) {
        bool carry_forward = false;
        ListNode *rethead(nullptr), *rettail(nullptr);
        while(l1 || l2 || carry_forward) {
            int out = (l1 ? l1->val : 0) + (l2 ? l2->val : 0) + (carry_forward ? 1 : 0);
            if(out > 9) {
                carry_forward = true;
                out = out % 10;
            } else {
                carry_forward = false;
            }
            // new node creation
            ListNode *new_digit = new ListNode(out);
            if(rethead == nullptr && rettail == nullptr) {
                // first digit
                rethead = new_digit;
                rettail = new_digit;
            } else {
                rettail->next = new_digit;
                rettail = new_digit;
            }

            // move fwd
            l1 = (l1) ? l1->next : nullptr;
            l2 = (l2) ? l2->next : nullptr;
        }
        return rethead;
    }

    typedef ListNode* (Solution::*RevFunc) (ListNode *head);
    RevFunc fptr;

public:
    ListNode* addTwoNumbers2(ListNode* l1, ListNode* l2) {
        this->fptr = &Solution::iterativeReverseLL;
        ListNode *r1, *r2;
        r1 = (ListNode *) (this->*fptr)(l1);
        r2 = (ListNode *) (this->*fptr)(l2);
        ListNode *r3 = this->addTwoReverse(r1, r2);
        return (ListNode *) (this->*fptr)(r3);
    }

    ListNode* addTwoNumbers(ListNode* l1, ListNode *l2) {
        using namespace std::placeholders;
        std::function<ListNode*(ListNode*)> fptr2 = std::bind(&Solution::iterativeReverseLL, this, _1);
        ListNode *r1, *r2;
        r1 = fptr2(l1);
        r2 = fptr2(l2);
        ListNode *r3 = this->addTwoReverse(r1, r2);
        return fptr2(r3);
    }
};
#endif //LEARN_CPP_SOLUTION_H
