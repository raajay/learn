//
// Created by Raajay on 1/9/17.
//

#include "Solution.h"

int main() {
    std::vector<int> a = {1, 4, 2,5,6};
    std::vector<int> b = {4,3,2,1};
    ListNode* l1 = createLinkedList(a);
    ListNode* l2 = createLinkedList(b);

    display(l1);
    Solution s;
    display(s.sortList(l1));

    display(l2);
    display(s.sortList(l2));

    return 0;
}