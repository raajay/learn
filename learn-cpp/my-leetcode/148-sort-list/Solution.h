//
// Created by Raajay on 1/9/17.
//

#ifndef LEARN_CPP_SOLUTION_H
#define LEARN_CPP_SOLUTION_H
#include "../util/util_structs.h"
#include <climits>

class Solution {

    ListNode* merge(ListNode* a, ListNode*b ) {
        ListNode retval(0);
        ListNode *iter = &retval;
        while(a != nullptr || b != nullptr) {
            if(!a && b) {
                iter->next = b;
                iter = b;
                b = b->next;
                continue;
            }

            if(a && !b) {
                iter->next = a;
                iter = a;
                a = a->next;
                continue;
            }

            // at this point both are not null
            if(a->val <= b->val) {
                iter->next = a;
                iter = a;
                a = a->next;
            } else {
                iter->next = b;
                iter = b;
                b = b->next;
            }
        }
        return retval.next;
    }

    ListNode* sort(ListNode* head) {
        // size 0
        if(head == nullptr)
            return nullptr;
        // size 1
        if(head->next == nullptr)
            return head;

        // split the list into two
        ListNode* slow = head;
        ListNode* fast = head;
        ListNode* prev = nullptr;
        while(fast != nullptr && fast->next != nullptr) {
            // will be executed at least once
            prev = slow;
            slow = slow->next;
            fast = fast->next->next;
        }

        ListNode* second = sort(slow);
        prev->next = nullptr;
        ListNode* first = sort(head);
        return merge(first, second);
    }


public:
    ListNode* sortList(ListNode* head) {
        // we need an O(n log n) sort
        return sort(head);
    }
};

#endif //LEARN_CPP_SOLUTION_H
