#include <iostream>
#include "solution.h"
#include <vector>


int main() {
//    std::vector<int> a = {2,4,3};
//    std::vector<int> b = {5,6,4};
    std::vector<int> a = {5};
    std::vector<int> b = {5};
    // int n1(a.size()), n2(b.size());

    ListNode* ll1(createLinkedList(a));
    ListNode* ll2(createLinkedList(b));

    display(ll1);
    display(ll2);

    Solution s;
    display(s.addTwoNumbers(ll1, ll2));
}
