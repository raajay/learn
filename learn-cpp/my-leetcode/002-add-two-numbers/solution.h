#include "../util/util_structs.h"

class Solution {
public:
    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
        // if one of them is zero return the other
//        if(l1->val == 0)
//            return l2;
//        if(l2->val == 0)
//            return l1;

        // init two iters
        ListNode* it1(l1);
        ListNode* it2(l2);
        bool carry_forward = false;

        ListNode* output(nullptr);
        ListNode* output_tail(nullptr);

        while(1) {

            int out = ((it1 != nullptr) ? it1->val : 0)
                + ((it2 != nullptr) ? it2->val : 0)
                + ((carry_forward)? 1 : 0);

            if(out > 9) {
                out = out % 10;
                carry_forward = true;
            } else {
                carry_forward = false;
            }

            ListNode* new_digit = new ListNode(out);

            if(output == nullptr) {
                output = new_digit;
                output_tail = new_digit;
            } else {
                // Add to the end
                output_tail->next = new_digit;
                output_tail = new_digit;
            }

            //
            it1 = (it1) ? it1->next : nullptr;
            it2 = (it2) ? it2->next : nullptr;

            // we break only when both lists reach an end and there is no carry
            // fwd
            if(it1 == nullptr && it2 == nullptr && !carry_forward)
                break;
        }

        return output;
    }
};
