// sol151.cpp

#include <iostream>
#include <string>
#include <vector>

class Solution {
public:
    void reverseWords(std::string &s) {
        // reverse the entire string first
        int i = 0; int j = s.length() - 1;
        while(i < j) {
            // swap characters at i and j
            char c = s[i];
            s[i] = s[j];
            s[j] = c;
            i++;
            j--;
        }

        // fix the white space errors (based on the question description)
        int n = s.length();
        i = 0; // last non space character index
        bool seen_first_space = false;
        bool seen_first_character = false;
        for(j =0; j < n ; j++) {
            if(s[j] == ' ' && (seen_first_space || !seen_first_character) ) {
                // skip and move ahead

            } else {
                seen_first_character = true;
                s[i] = s[j]; 
                i++;
                if(s[j] == ' ') {
                    seen_first_space = true;
                } else {
                    seen_first_space = false;
                }
            }
        }
        s = s.substr(0, i);
        n = s.length();
        std::cout << "s=" << s <<  " i=" << i << std::endl;

        // then reverse each word
        i = 0; j = 0; 
        for(; j <= n ; j++) {
            if(s[j] == ' ' || j == n) {
                // swap everything between i and j
                int k = j-1;
                while(i < k) {
                    char c = s[i];
                    s[i] = s[k];
                    s[k] = c;
                    k--;
                    i++;
                }
                i = j+1;
            } 
        }
        std::cout << s << std::endl;

    }
};

int check(std::string &input, std::string expected) {
    Solution s;
    s.reverseWords(input);
    if(expected.compare(input) != 0) {
        std::cout << input << std::endl;
        std::cout << expected << std::endl;
        return 1;
    } else {
        return 0;
    }
}


int main() {
    int errors = 0;
    std::string s1 = "a";
    std::string o1 = "a";
    errors += check(s1, o1);
    std::cout << errors << std::endl;
    return 0;
}