//
// Created by Raajay Viswanathan
//
#ifndef LEETCODE_TEST_SOLUTION_015_H
#define LEETCODE_TEST_SOLUTION_015_H

#include <iostream>
#include "solution.hpp"
#include <vector>

class TestSolution015 {

    int check(std::vector<int> nums) {
        Solution015 s;
        s.solve(nums);
        return 0;
    }

public:
    void runTests() {
        int errors = 0;

        // fill in the test cases

        if(errors == 0) {
            std::cout << "All tests passed." << std::endl;
        }
    }
};

#endif
