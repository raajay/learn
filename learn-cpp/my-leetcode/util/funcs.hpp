#ifndef UTIL_FUNCS_H
#define UTIL_FUNCS_H

#include <iostream>
#include <vector>

void displayVec(std::vector<int> a) {
    for(size_t i = 0; i < a.size(); i++) {
        std::cout << a[i] << " ";
    }
    std::cout << std::endl;
}

#endif
