//
// Created by Raajay on 1/8/17.
//

#ifndef LEARN_CPP_UTIL_STRUCTS_H
#define LEARN_CPP_UTIL_STRUCTS_H

#include <iostream>
#include <vector>

struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(nullptr) {}
};

void display(ListNode* head) {
    ListNode* iter = head;
    while(iter != nullptr) {
        std::cout << iter->val << ",";
        iter = iter->next;
    }
    std::cout << std::endl;
}


ListNode* createLinkedList(std::vector<int> a) {
    int n(a.size());
    ListNode* head = nullptr;
    for(int i = 0; i < n; i++) {
        ListNode* new_node = new ListNode(a[n-i-1]);
        new_node->next = head;
        head = new_node;
    }
    return head;
}

#endif //LEARN_CPP_UTIL_STRUCTS_H
