#include <iostream>
#include <vector>

class Solution {
public:
    int searchRecursive(std::vector<int> &nums, int target, int start, int end) {

        /* Find if the target is present between start and end, both inclusive*/
        if(end == start) {
            return (nums[start] == target) ? start : -1;
        }

        int mid = (start + end) / 2;
        if(nums[mid] == target) return mid;

        // if(target > nums[mid] && )


        return -1;
    }


    int search(std::vector<int>& nums, int target) {
        return searchRecursive(nums, target, 0, nums.size() - 1);
    }
};

int main() {
    return 0;
}