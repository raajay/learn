//
// Created by Raajay on 1/8/17.
//
#include "Solution.h"

int main() {
    std::vector<int> a = {1,2,3,4,5};
    std::vector<int> b = {1,2,3,4};
    ListNode* l1 = createLinkedList(a);
    ListNode* l2 = createLinkedList(b);

    display(l1);
    Solution s;
    s.reorderList(l1);
    display(l1);

    display(l2);
    s.reorderList(l2);
    display(l2);

    return 0;
}
