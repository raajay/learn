//
// Created by Raajay on 1/8/17.
//

#ifndef LEARN_CPP_SOLUTION_H
#define LEARN_CPP_SOLUTION_H
#include "../util/util_structs.h"

class Solution {

    bool simpleRecurse(ListNode* iter) {
        if(iter == nullptr) {
            return false;
        } else {
            // recurse
            bool should_stop = simpleRecurse(iter->next);
            if(should_stop) {
                return true;
            }
            else {
                if(iter == curr) {
                    iter->next = nullptr;
                    return true;
                }
                if(curr->next == iter) {
                    iter->next = nullptr;
                    return true;
                }

                iter->next = curr->next;
                curr->next = iter;
                curr = iter->next;
                return false;
            }
        }
    }

    ListNode* curr;

public:
    void reorderList(ListNode* head) {
        curr = head;
        simpleRecurse(head);
    }
};

#endif //LEARN_CPP_SOLUTION_H
