//
// Created by Raajay Viswanathan
//
#ifndef LEETCODE_TEST_SOLUTION_295_H
#define LEETCODE_TEST_SOLUTION_295_H

#include <iostream>
#include "solution.hpp"

class TestSolution295 {

    int check() {
        Solution295 s;
        s.solve();

        // compare expected and actual result
        return 0;
    }

public:
    void runTests() {
        int errors = 0;

        // fill in the test cases
        if(errors == 0) {
            std::cout << "All tests passed." << std::endl;
        }
    }
};

#endif
