//
// Created by Raajay Viswanathan
//
#include "test.hpp"
#include <vector>
#include <algorithm>

using namespace std;

bool clarge(int a, int b)  {
    return a > b;
}

bool csmall(int a, int b)  {
    return a < b;
}

class MedianFinder {

    vector<int> *small_heap; // max-heap
    vector<int> *large_heap; // min-heap

public:
    /** initialize your data structure here. */
    MedianFinder() {
        small_heap = new vector<int>();
        large_heap = new vector<int>();

    }

    void addNum(int num) {

        if(large_heap->size() == 0 || num < large_heap->front()) {
            // push to small heap
            small_heap->push_back(num); push_heap(small_heap->begin(), small_heap->end(), csmall);

            // if small_heap size exceeds large_heap size by greater than 1
            // extract max and push to large heap
            if(small_heap->size() - large_heap->size() > 1) {
                int max_small = small_heap->front();
                pop_heap(small_heap->begin(), small_heap->end(), csmall); small_heap->pop_back();

                large_heap->push_back(max_small); push_heap(large_heap->begin(), large_heap->end(), clarge);
            }

        } else {
            // push to large heap
            large_heap->push_back(num); push_heap(large_heap->begin(), large_heap->end(), clarge);

            // if large_heap size exceeds small_heap size extract min and push to small heap
            if(large_heap->size() - small_heap->size() > 0) {
                int min_large = large_heap->front();
                pop_heap(large_heap->begin(), large_heap->end(), clarge); large_heap->pop_back();

                small_heap->push_back(min_large); push_heap(small_heap->begin(), small_heap->end(), csmall);
            }
        }

    }

    double findMedian() {

        if(small_heap->size() == large_heap->size()) {
            return (small_heap->front() + large_heap->front()) / 2.0;
        } else {
            return small_heap->front();
        }

    }
};

/**
 * Your MedianFinder object will be instantiated and called as such:
 * MedianFinder obj = new MedianFinder();
 * obj.addNum(num);
 * double param_2 = obj.findMedian();
 */

int main() {
    MedianFinder obj;
    obj.addNum(1);
    obj.addNum(1);
    obj.addNum(2);
    std::cout << obj.findMedian() << std::endl;;
    return 0;
}
