#include "../util/util_structs.h"
#include <unordered_map>

class Solution {

    // O(n) space implementation
    ListNode* cycleOn(ListNode* head) {
        std::unordered_map<ListNode*, bool> seen;
        while(head) {
            if(seen.find(head) != seen.end()) {
                break;
            }
            seen[head] = true;
            head = head->next;
        }
        return head;
    }

    // O(1) space implementation
    ListNode* cycleO1(ListNode* head) {
        if(!head || !(head->next)) {
            return nullptr;
        }
        ListNode *fast(head), *slow(head);
        do {
            slow = slow->next;
            fast = fast->next->next;
        } while(fast != slow && fast && fast->next);

        if(fast != slow) {
            return nullptr;// no cycle
        } else {
            ListNode *pacer(head);
            while(pacer == slow) {
                pacer = pacer->next;
                slow = slow->next;
            }
            return pacer;
        }
    }


public:
    ListNode *detectCycle(ListNode *head) {
        return cycleOn(head);
    }
};
