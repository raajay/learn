#include <iostream>
#include "Solution.h"

int main() {
    std::vector<int> a = {1,2,3,4,5};
    ListNode* l1 = createLinkedList(a);
    display(l1);
    Solution s;
    ListNode* r1 = s.detectCycle(l1);
    display(r1);
    return 0;
}
