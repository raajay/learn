#include <algorithm>
#include <vector>

class p90Solution {
 public:
  int numCurrent(std::vector<int> &current, int value) {
    int retval = 0;
    int n = current.size();
    for (int i = n - 1; i >= 0; i--) {
      if (current[i] == value) {
        retval++;
        continue;
      }
      break;
    }
    return retval;
  }

  std::vector<std::vector<int>> subsetsWithDup(std::vector<int> &nums) {
    std::sort(nums.begin(), nums.end());

    std::vector<std::vector<int>> retval;
    // always has an empty set
    retval.push_back(std::vector<int>());
    int curr_val_idx = 0;

    for (int i = 0; i < nums.size(); i++) {

      if (0 == i || nums[i] != nums[i - 1]) {
        curr_val_idx = 0;
      } else {
        curr_val_idx++;
      }

      std::vector<std::vector<int>> passval;

      for (auto current : retval) {
        int nc = numCurrent(current, nums[i]);
        if (nc == curr_val_idx) {
          std::vector<int> candidate(current);
          candidate.push_back(nums[i]);
          passval.push_back(candidate);
        }
      }

      for (auto vv : passval) {
        retval.push_back(vv);
      }
    }
    return retval;
  }
};
