#include "p198.hpp"
#include <gtest/gtest.h>

TEST(p198SolutionTest, ProvidedTestCase) {
  p198Solution sol;
  std::vector<int> input = {1, 2, 3, 4};
  EXPECT_EQ(sol.rob(input), 6);
}

TEST(p198SolutionTest, Case2) {
  p198Solution sol;
  std::vector<int> input = {4, 3, 2, 1};
  EXPECT_EQ(sol.rob(input), 6);
}
