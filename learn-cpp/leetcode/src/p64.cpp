#include "p64.hpp"
#include <gtest/gtest.h>

TEST(p64SolutionTest, ProvidedTestCase) {
  p64Solution sol;
  std::vector<std::vector<int>> input;
  input.push_back(std::vector<int>({1, 3, 1}));
  input.push_back(std::vector<int>({1, 5, 1}));
  input.push_back(std::vector<int>({4, 2, 1}));
  EXPECT_EQ(sol.minPathSum(input), 7);
}
