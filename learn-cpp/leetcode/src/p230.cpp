#include "p230.hpp"
#include <gtest/gtest.h>

TEST(p230SolutionTest, ProvidedTestCase) {
  p230Solution sol;
  std::vector<int> a{2, 1};
  TreeNode *root = createBST(a);
  EXPECT_EQ(sol.kthSmallest(root, 2), 2);
  EXPECT_EQ(sol.kthSmallest(root, 1), 1);
}
