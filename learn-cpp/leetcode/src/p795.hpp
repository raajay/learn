#include <iostream>
#include <vector>

class p795Solution {
 public:
  int numSubarrayBoundedMax(std::vector<int>& A, int L, int R) {
    int n = A.size();
    // number of sub-arrays which end with element i
    std::vector<int> nums(n);

    nums[0] = (A[0] >= L && A[0] <= R) ? 1 : 0;

    // O (n) algorithm
    for (int i = 1; i < n; i++) {
      nums[i] = 0;
      if (A[i] > R) {
        continue;
      }

      // number of sub-arrays = num of sub arrays ending with A[i-1] and max in
      // range [L, R]
      nums[i] += nums[i - 1];

      if (A[i] >= L) {
        // the element alone can be a sub-array
        nums[i] += 1;
      }
    }
    int s = 0;
    for (int i = 0; i < n; i++) {
      s += nums[i];
    }
    return s;
  }
};
