#include "p6.hpp"
#include <gtest/gtest.h>

TEST(p6SolutionTest, ProvidedTestCase) {
  p6Solution sol;
  std::string input = "PAYPALISHIRING";
  std::string expected_output = "PAHNAPLSIIGYIR";
  std::string output = sol.convert(input, 3);
  EXPECT_STREQ(expected_output.c_str(), output.c_str());
}
