// google phone interview
#include <cassert>
#include <gtest/gtest.h>
#include <stack>
#include <vector>

bool isInBound(int i, int j, int num_rows, int num_cols) {
  return i >= 0 && i < num_rows && j >= 0 && j < num_cols;
}

bool isGoSurrounded(std::vector<std::vector<int>> &matrix, int i, int j) {
  int n = matrix.size();
  int m = matrix[0].size();
  assert(isInBound(i, j, n, m));
  assert(0 == matrix[i][j]);
  std::vector<std::vector<bool>> visited(n, std::vector<bool>(m, false));
  bool isSurrounded = false;
  std::stack<std::pair<int, int>> ss;
  ss.push(std::pair<int, int>(i, j));
  while (!ss.empty()) {
    auto point = ss.top();
    ss.pop();
    visited[point.first][point.second] = true;
    assert(matrix[point.first][point.second] == 0);
    for (int r = i - 1; r <= i + 1; r++) {
      for (int c = j - 1; c <= j + 1; c++) {
        if (r == i && c == j)
          continue;
        if (!isInBound(r, c, n, m))
          continue;
        // neighboring empty
        if (matrix[r][c] == -1)
          return false;
        // do no pursue
        if (matrix[r][c] == 1)
          continue;
        if (visited[r][c])
          continue;
        // push to visit later
        ss.push(std::pair<int, int>(r, c));
      }
    }
  }
  return true;
}

TEST(GoSurround, Case0) {
  std::vector<std::vector<int>> board = {{1, 1, 1}, {1, 0, 1}, {1, 1, 1}};
  EXPECT_EQ(true, isGoSurrounded(board, 1, 1));
}

TEST(GoSurround, Case1) {
  std::vector<std::vector<int>> board = {{1, 1, -1}, {1, 0, 1}, {1, 1, 1}};
  EXPECT_EQ(false, isGoSurrounded(board, 1, 1));
}

TEST(GoSurround, Case2) {
  std::vector<std::vector<int>> board = {
      {1, 1, 1, 1}, {1, 0, 0, 0}, {1, 0, 0, 1}, {1, 1, 1, 1}};
  EXPECT_EQ(true, isGoSurrounded(board, 1, 1));
}

// TEST(GoSurround, Case3) {
// std::vector<std::vector<int>> board = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}};
// EXPECT_EQ(false, isGoSurrounded(board, 0, 0));
// EXPECT_EQ(false, isGoSurrounded(board, 0, 1));
// EXPECT_EQ(false, isGoSurrounded(board, 0, 2));
// EXPECT_EQ(false, isGoSurrounded(board, 1, 0));
// }
