/**
 * Given a boggle board and a set of words find all the words that exist in the
 * board.
 */

#if 0
#include "datastructs/trie.hpp"
#include <stack>
#include <string>
#include <unordered_map>
#include <vector>

struct StackEntry {
 public:
  StackEntry(int i1, int j1, TrieNode *node1) : i(i1), j(j1), node(node1) {}
  const int i;
  const int j;
  const TrieNode *node;
};

std::vector<std::string> findWords(std::vector<std::string> board,
                                   std::vector<std::string> words) {
  int n = board.size();
  int m = board[0].size();
  // construct a trie from the words
  TrieNode *root = new TrieNode();
  for (auto w : words) {
    addWord(root, w);
  }

  // Then do DFS; follow edges if children are present in the trie node. Keep
  // track of characters traversed, and check at each node if the word is
  // present in dictionary.

  std::stack<StackEntry> ss;
  ss.push(StackEntry(0, 0, root));
  destroyTrie(root);
}
#endif
