#include "datastructs/tree.hpp"
#include <algorithm>
#include <gtest/gtest.h>
#include <vector>

// O(n log (n)) for sorting
std::vector<int> intersection(std::vector<int> a, std::vector<int> b) {
  std::sort(a.begin(), a.end());
  std::sort(b.begin(), b.end());
  std::vector<int> retval;
  int i = 0, j = 0;
  while (i < a.size() && j < b.size()) {

    if (a[i] == b[j]) {
      retval.push_back(a[i]);
      i++;
      j++;
      continue;
    }

    if (a[i] > b[j]) {
      j++;
    } else {
      i++;
    }
  }
  return retval;
}

class TestIntersection : public ::testing::Test {};

TEST_F(TestIntersection, Case0) {
  std::vector<int> a = {1, 3, 5, 7, 9};
  std::vector<int> b = {2, 4, 6, 8, 9};
  std::vector<int> solution = {9};
  EXPECT_VEC_EQ(intersection(a, b), solution);
}

TEST_F(TestIntersection, Case1) {
  std::vector<int> a = {1, 2, 3, 4, 5};
  std::vector<int> b = {1, 2, 3, 4, 5};
  std::vector<int> solution = {1, 2, 3, 4, 5};
  EXPECT_VEC_EQ(intersection(a, b), solution);
}
