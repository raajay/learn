// google onsite interview
#include <cassert>
#include <gtest/gtest.h>
#include <vector>

class OrderingBuffer {
 public:
  OrderingBuffer(uint32_t max_out_of_order)
      : capacity(max_out_of_order), readHead(0), bytesRead(0) {
    buffer = new char[max_out_of_order];
    unread = new bool[max_out_of_order];
    for (int i = 0; i < max_out_of_order; i++) {
      unread[i] = false;
    }
  }

  ~OrderingBuffer() {
    delete[] buffer;
    delete[] unread;
  }

  uint32_t read(char* buf, uint32_t len) {
    uint32_t i = 0;
    while (i < len && unread[readHead] == true) {
      buf[i] = buffer[readHead];
      unread[readHead] = false;
      i++;
      bytesRead++;
      readHead = (readHead + 1) % capacity;
    }
    return i;
  }

  bool write(char* buf, uint32_t len, uint32_t offset) {
    if (offset + len - bytesRead > capacity)
      return false;
    int wIdx = (readHead + offset - bytesRead) % capacity;
    int i = 0;
    while (i < len) {
      assert(unread[wIdx] == false);
      buffer[wIdx] = buf[i];
      unread[wIdx] = true;
      wIdx = (wIdx + 1) % capacity;
      i++;
    }
    return true;
  }

 private:
  uint32_t capacity;
  uint32_t readHead;
  uint32_t bytesRead;
  char* buffer;
  bool* unread;
};

class TestOrderingBuffer : public ::testing::Test {};

TEST_F(TestOrderingBuffer, OrderedWrites) {
  OrderingBuffer b(100);
  char tmp[4];
  for (uint32_t i = 0; i < 25; i++) {
    for (uint32_t j = 0; j < 4; j++) {
      tmp[j] = (char)(4 * i + j);
    }
    EXPECT_EQ(b.write(tmp, 4, 4 * i), true);
  }
  for (int i = 0; i < 25; i++) {
    EXPECT_EQ(b.read(tmp, 4), 4);
  }
  for (uint32_t i = 26; i < 30; i++) {
    for (uint32_t j = 0; j < 4; j++) {
      tmp[j] = (char)(4 * i + j);
    }
    EXPECT_EQ(b.write(tmp, 4, 4 * i), true);
  }
}

TEST_F(TestOrderingBuffer, ReverseWrites) {
  OrderingBuffer b(100);
  char tmp[4];
  for (int32_t i = 24; i >= 0; i--) {
    for (uint32_t j = 0; j < 4; j++) {
      tmp[j] = (char)(4 * i + j);
    }
    EXPECT_EQ(b.write(tmp, 4, 4 * i), true);
  }
  for (uint32_t i = 0; i < 25; i++) {
    EXPECT_EQ(b.read(tmp, 4), 4);
    for (uint32_t j = 0; j < 4; j++) {
      EXPECT_EQ(tmp[j], (char)(4 * i + j));
    }
  }
}
