#include "p795.hpp"
#include <gtest/gtest.h>

TEST(p795SolutionTest, ProvidedTestCase) {
  p795Solution sol;
  std::vector<int> A = {2, 1, 4, 3};
  EXPECT_EQ(sol.numSubarrayBoundedMax(A, 2, 3), 3);
}

TEST(p795SolutionTest, Failed1) {
  p795Solution sol;
  std::vector<int> A = {73, 55, 36, 5, 55, 14, 9, 7, 72, 52};
  EXPECT_EQ(sol.numSubarrayBoundedMax(A, 32, 69), 22);
}
