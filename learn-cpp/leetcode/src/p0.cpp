#include <gtest/gtest.h>

#include "p0.hpp"
#include <unordered_map>
#include <vector>

TEST(UnitTest, Unit) { EXPECT_EQ(unit(), 0); }

TEST(VectorInit, Case1) {
  // init with basic assignment
  std::vector<int> a = {1, 2, 3};
  EXPECT_EQ(a[0], 1);
  EXPECT_EQ(a[1], 2);
  EXPECT_EQ(a[2], 3);
}

TEST(VectorInit, Case2) {
  // init with list abstraction passed to constructor
  std::vector<int> a({1, 2, 3});
  EXPECT_EQ(a[0], 1);
  EXPECT_EQ(a[1], 2);
  EXPECT_EQ(a[2], 3);
}

TEST(VectorInit, Case3) {
  // init with same value
  std::vector<int> a(3, 25);
  EXPECT_EQ(a[0], 25);
  EXPECT_EQ(a[1], 25);
  EXPECT_EQ(a[2], 25);
}

TEST(VectorInit, Case4) {
  // 2 - D init, with same row value
  std::vector<std::vector<int>> a(3, {1, 2, 3});
  EXPECT_EQ(a[0][0], 1);
  EXPECT_EQ(a[1][0], 1);
  EXPECT_EQ(a[1][1], 2);
  EXPECT_EQ(a[2][1], 2);
  EXPECT_EQ(a[2][2], 3);
}

TEST(VectorInit, Case5) {
  // 2 - D init, with same row value
  std::vector<std::vector<int>> a = {{1, 2, 3}, {4, 5, 6}};
  EXPECT_EQ(a[0][0], 1);
  EXPECT_EQ(a[0][1], 2);
  EXPECT_EQ(a[0][2], 3);
  EXPECT_EQ(a[1][0], 4);
  EXPECT_EQ(a[1][1], 5);
  EXPECT_EQ(a[1][2], 6);
}

TEST(VectorInit, Case6) {
  std::vector<int> a{1, 2, 3};
  EXPECT_EQ(a[0], 1);
  EXPECT_EQ(a[1], 2);
  EXPECT_EQ(a[2], 3);
}

TEST(UnorderedMapInit, Case1) {
  std::unordered_map<int, std::vector<int>> a = {
      {1, {2, 3, 4}}, {2, {1, 3, 4}}, {3, {1, 2, 4}}, {4, {1, 2, 3}}};
  EXPECT_EQ(a[1][0], 2);
  EXPECT_EQ(a[1][1], 3);
  EXPECT_EQ(a[1][2], 4);
  EXPECT_EQ(a[4][0], 1);
  EXPECT_EQ(a[4][1], 2);
  EXPECT_EQ(a[4][2], 3);
}

// TODO(raajay):
// learn about rvalue reference in C++
// http://thbecker.net/articles/rvalue_references/section_01.html/
//
// TODO(raajay): TLS or SSL handshake
// https://www.ibm.com/support/knowledgecenter/en/SSFKSJ_7.1.0/com.ibm.mq.doc/sy10660_.htm
// 2. Learn about how client typically fetches data from the server
//
//
//
// TODO(raajay): SPDY
// HTTP/1.1 --> SPDY --> HTTP/2
// How SPDY improved over HTTP/1.1?
// 1. Parallel requests for resources. HTTP/1.1 can fetch only one resource at a
// time.
// 2. Server side push. One a HTTP connection is established, the server can
//    push data. Traditionally, in HTTP all the fetch for resources are
//    client initiated.
// 3. Data compression. By default all data is compressed.
// 4. Redundant headers. HTTP repeats headers over the course of a session. SPDY
// removes unneccessary headers.

// TODO(raajay): TCP Fast Open
//
//
// TODO(raajay): QUIC
// https://ma.ttias.be/googles-quic-protocol-moving-web-tcp-udp/
