#include "p72.hpp"
#include <gtest/gtest.h>

TEST(p72SolutionTest, AllocationTest) {
  int n = 3;
  int m = 4;
  std::vector<std::vector<int>> memory(n, std::vector<int>(m, 4));

  for (int i = 0; i < n; i++) {
    for (int j = 0; j < m; j++) {
      EXPECT_EQ(memory[i][j], 4);
    }
  }

  for (int i = 0; i < n; i++) {
    for (int j = 0; j < m; j++) {
      memory[i][j] = i + j;
    }
  }

  for (int i = 0; i < n; i++) {
    for (int j = 0; j < m; j++) {
      EXPECT_EQ(memory[i][j], i + j);
    }
  }
}

TEST(p72SolutionTest, ProvidedTestCase) {
  p72Solution sol;
  EXPECT_EQ(sol.minEditDistance("ABC", "XYZ"), 3);
  EXPECT_EQ(sol.minEditDistance("ABC", "DEFABC"), 3);
  EXPECT_EQ(sol.minEditDistance("ABCDEF", "AXBCDE"), 2);
}
