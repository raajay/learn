#include "p90.hpp"
#include <gtest/gtest.h>
#include <vector>

TEST(ProvidedTestCase, Case1) {

  std::vector<int> input;
  input.push_back(1);
  input.push_back(2);
  input.push_back(2);

  p90Solution sol;
  sol.subsetsWithDup(input);
}
