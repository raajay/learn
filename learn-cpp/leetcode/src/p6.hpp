#include <iostream>
#include <sstream>

class p6Solution {
 public:
  std::string convert(std::string s, int numRows) {
    std::stringstream ss;
    int n = s.size();

    for (int k = 0; k < numRows; k++) {
      int i = k;
      bool first = true;
      while (i < n) {
        ss << s.at(i);

        if (k == 0 || k == numRows - 1) {
          i += 2 * (numRows - 1);
          continue;
        }

        if (first) {
          i += 2 * (numRows - k - 1);
        } else {
          i += 2 * k;
        }

        first = !first;
      }
    }
    return ss.str();
  }
};
