#include <algorithm>
#include <iostream>
#include <vector>

bool nextPermutation(std::vector<int> &nums) {
  int n = nums.size();

  int i;
  for (i = n - 1; i >= 1; i--) {
    if (nums[i] > nums[i - 1]) {
      break;
    }
  }

  if (i == 0)
    return false;

  // i is the peak, all values to right of i are <= nums[i]
  // value at i-1 is strictly less than nums[i]
  int j;
  for (j = n - 1; j >= i; j--) {
    if (nums[j] > nums[i - 1]) {
      break;
    }
  }

  int tmp;
  tmp = nums[i - 1];
  nums[i - 1] = nums[j];
  nums[j] = tmp;

  // flip all bits from i to  end
  int i1 = i;
  int i2 = n - 1;
  while (i1 < i2) {
    tmp = nums[i1];
    nums[i1] = nums[i2];
    nums[i2] = tmp;
    i1++;
    i2--;
  }
}

void displayVec(const std::vector<int> &nums) {
  for (auto a : nums) {
    std::cout << a << " ";
  }
  std::cout << std::endl;
}

int printPermuations(std::vector<int> nums) {
  int n = nums.size();
  std::sort(nums.begin(), nums.end());
  int counter = 0;
  do {
    displayVec(nums);
    counter++;
  } while (nextPermutation(nums));
  return counter;
}
