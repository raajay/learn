#include "misc/stacks.hpp"
#include <gtest/gtest.h>

TEST(Parantheses, CorrectnessCase) {
  EXPECT_EQ(areBracesCorrect("[()}"), false);
  EXPECT_EQ(areBracesCorrect("[()]"), true);
  EXPECT_EQ(areBracesCorrect("[(()()()()[()]}}"), false);
  EXPECT_EQ(areBracesCorrect(")}]"), false);
  EXPECT_EQ(areBracesCorrect("[({"), false);
}
