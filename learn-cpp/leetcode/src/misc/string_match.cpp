#include "misc/string_match.hpp"
#include "datastructs/tree.hpp"
#include <gtest/gtest.h>

TEST(StringMatch, KMPPreprocess0) {
  std::string word = "aabaabaaa";
  std::vector<int> shift(word.length(), 0);
  preprocessKMP(word, shift);
  std::vector<int> solution = {0, 1, 0, 1, 2, 3, 4, 5, 2};
  EXPECT_VEC_EQ(shift, solution);
}

TEST(StringMatch, KMPCase0) {
  std::string word = "abcab";
  std::string text = "abcabcab";
  std::vector<int> solution = {0, 3};
  EXPECT_VEC_EQ(kmp(text, word), solution);
}
