#include <iostream>
#include <stack>
#include <vector>

bool matchParantheses(char a, char b) {
  if (a == '{') {
    return b == '}';
  }
  if (a == '(') {
    return b == ')';
  }
  if (a == '[') {
    return b == ']';
  }
  if (a == '}') {
    return b == '{';
  }
  if (a == ')') {
    return b == '(';
  }
  if (a == ']') {
    return b == '[';
  }
  return false;
}

bool areBracesCorrect(std::string s) {
  int n = s.length();
  std::stack<char> braces;
  for (int i = 0; i < n; i++) {
    if (s[i] == '{' || s[i] == '(' || s[i] == '[') {
      braces.push(s[i]);
    } else if (s[i] == '}' || s[i] == ')' || s[i] == ']') {
      if (braces.empty() || !matchParantheses(braces.top(), s[i])) {
        return false;
      }
      braces.pop();
    }
  }
  return braces.empty();
}
