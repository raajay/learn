#include <algorithm>
#include <string>
#include <unordered_map>
#include <vector>

/**
 * Find the number of contiguous sub-sequences that sum to K
 */
int findNumContiguousSum(std::vector<int> nums, int target) {
  int n = nums.size();
  int runningSum = 0;
  int retval = 0;
  std::unordered_map<int, int> sumCount;
  for (int i = 0; i < n; i++) {
    runningSum += nums[i];
    if (runningSum == target) {
      retval++;
    }
    int diff = runningSum - target;
    bool found = sumCount.find(diff) != sumCount.end();
    retval += (found ? sumCount[diff] : 0);
    if (sumCount.find(runningSum) != sumCount.end()) {
      sumCount[runningSum] += 1;
    } else {
      sumCount[runningSum] = 1;
    }
  }
  return retval;
}

/**
 * Find the number of contiguous sub-sequences that sum to K.
 * Brute force method
 */
int findNumContiguousSumBrute(std::vector<int> nums, int target) {
  int n = nums.size();
  int retval = 0;
  for (int i = 0; i < n; i++) {
    for (int j = i; j < n; j++) {
      int s = 0;
      for (int k = i; k <= j; k++) {
        s += nums[k];
      }
      if (s == target) {
        retval++;
      }
    }
  }
  return retval;
}

/**
 * Find the length of longest increasing subsequence.
 * This is an O(n^2) algorithm with O(n) space.
 */
int longestIncreasingSubsequence(std::vector<int> nums) {
  int n = nums.size();
  std::vector<int> lengths(n, 0);
  lengths[0] = 1;
  for (int i = 1; i < n; i++) {
    int curr_len = 1;  // the element only
    for (int j = 0; j < i; j++) {
      if (nums[i] > nums[j]) {
        curr_len = std::max(curr_len, lengths[j] + 1);
      }
    }
    lengths[i] = curr_len;
  }
  int retval = 0;
  for (int i = 0; i < n; i++) {
    retval = std::max(retval, lengths[i]);
  }
  return retval;
}

void swapValAtIndices(std::vector<int> &nums, int i, int j) {
  int tmp = nums[i];
  nums[i] = nums[j];
  nums[j] = tmp;
}

/**
 * Pivot an array around the provided key. Returns the index of the pivot.  All
 * indices less than returned value should have value strictly less than the
 * pivot key. All indices greater than pivot should be greater than or equal to
 * the pivot key.
 */
int pivot(std::vector<int> &nums, int pivot_key) {
  int i = 0;
  int j = nums.size();
  while (i < j) {
    if (nums[i] < pivot_key && nums[j] >= pivot_key) {
      i++;
      j--;
    } else if (nums[i] < pivot_key && nums[j] < pivot_key) {
      swapValAtIndices(nums, i + 1, j);
      i++;
    } else if (nums[i] >= pivot_key && nums[j] >= pivot_key) {
      swapValAtIndices(nums, i, j - 1);
      j--;
    } else {
      swapValAtIndices(nums, i, j);
      i++;
      j--;
    }
  }
  return i;
}

bool isTriangle(int a, int b, int c) {
  std::vector<int> tmp = {a, b, c};
  std::sort(tmp.begin(), tmp.end());
  return tmp[2] < (tmp[0] + tmp[1]);
}

int findNumTriangles(std::vector<int> nums) {
  int n = nums.size();
  if (n < 3)
    return 0;
  std::sort(nums.begin(), nums.end());
  int counter = 0;
  for (int smallIndex = 0; smallIndex < n - 2; smallIndex++) {
    int midIndex = smallIndex + 1;
    int largeIndex = smallIndex + 2;
    while (midIndex < n - 1) {
      if (largeIndex == n ||
          nums[largeIndex] >= nums[smallIndex] + nums[midIndex]) {
        counter += (largeIndex - midIndex - 1);
        midIndex++;
      } else {
        largeIndex++;
      }
    }
  }
  return counter;
}

int findNumTrianglesBrute(std::vector<int> nums) {
  int n = nums.size();
  int counter = 0;
  for (int i = 0; i < n; i++) {
    for (int j = i + 1; j < n; j++) {
      for (int k = j + 1; k < n; k++) {
        if (isTriangle(nums[i], nums[j], nums[k])) {
          counter++;
        }
      }
    }
  }
  return counter;
}

int lengthCommonSubsequence(std::string s1, std::string s2) {
  int n = s1.length();
  int m = s2.length();
  if (0 == n || 0 == m)
    return 0;
  std::vector<std::vector<int>> lcs(n, std::vector<int>(m, 0));
  // set first row
  bool found = false;
  for (int j = 0; j < m; j++) {
    if (s1[0] == s2[j])
      found = true;
    lcs[0][j] = (found) ? 1 : 0;
  }
  found = false;
  for (int i = 0; i < n; i++) {
    if (s1[i] == s2[0])
      found = true;
    lcs[i][0] = (found) ? 1 : 0;
  }
  for (int i = 1; i < n; i++) {
    for (int j = 1; j < m; j++) {
      lcs[i][j] = std::max(lcs[i - 1][j], lcs[i][j - 1]);
      if (s1[i] == s2[j]) {
        lcs[i][j] = std::max(1 + lcs[i - 1][j - 1], lcs[i][j]);
      } else {
        lcs[i][j] = std::max(lcs[i - 1][j - 1], lcs[i][j]);
      }
    }
  }
  return lcs[n - 1][m - 1];
}

// TODO(raajay): incorrect, fix this
int lengthLongestRepeatedSequence(std::string s1) {
  int n = s1.length();
  std::vector<int> mem(n, 0);
  for (int i = 0; i < n; i++) {
    // find the prior occurence of current character
    int j = i - 1;
    while (j >= 0) {
      if (s1[j] == s1[i])
        break;
      j--;
    }
    if (j < 0) {
      continue;
    }
    // for all characters before j find the largest value in mem
    j--;
    int max_val = 0;
    while (j >= 0) {
      max_val = std::max(max_val, mem[j]);
      j--;
    }
    mem[i] = 1 + max_val;
  }
  auto max_element = std::max_element(mem.begin(), mem.end());
  return *max_element;
}

int findMaxContiguousSequence(std::vector<int> &nums) {
  int retval = 0;
  int t = 0;
  for (auto x : nums) {
    t += x;
    if (t < 0) {
      t = 0;
    }
    retval = std::max(retval, t);
  }
  return retval;
}
