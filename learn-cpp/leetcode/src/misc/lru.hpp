#include <cassert>
#include <string>
#include <unordered_map>

class LRUCache {
 public:
  LRUCache(int capacity)
      : cacheCapacity(capacity), cacheSize(0), head(nullptr), tail(nullptr) {}

  void write(std::string key, std::string value) {
    if (cacheSize >= cacheCapacity) {
      overwriteLRUData(key, value);
      touch(lookup[key]);
    } else {
      write0(key, value);
    }
  }

  bool isCached(std::string key) { return lookup.find(key) != lookup.end(); }

  std::string read(std::string key) {
    touch(lookup[key]);
    return lookup[key]->value;
  }

 private:
  class CacheEntry {
   public:
    CacheEntry(std::string k, std::string v)
        : key(k), value(v), next(nullptr), prev(nullptr) {}
    std::string key;
    std::string value;
    CacheEntry *next;
    CacheEntry *prev;
  };

  // write a new entry at head
  CacheEntry *write0(std::string key, std::string value) {
    CacheEntry *entry = new CacheEntry(key, value);
    lookup[key] = entry;

    if (nullptr == head) {
      tail = entry;
    } else {
      head->prev = entry;
      entry->next = head;
    }

    head = entry;
    this->cacheSize++;
    return entry;
  }

  void overwriteLRUData(std::string key, std::string value) {
    // remove tail
    auto lrukey = tail->key;
    lookup.erase(lrukey);
    tail->key = key;
    tail->value = value;
    lookup[key] = tail;
  }

  void touch(CacheEntry *entry) {
    if (entry->prev == nullptr) {
      // already at head
      return;
    }

    // unlink the node (can be at tail or middle)
    if (entry->next) {
      entry->next->prev = entry->prev;
    } else {
      assert(tail == entry);
      tail = entry->prev;
    }

    entry->prev->next = entry->next;

    // insert at head
    entry->next = head;
    entry->prev = nullptr;
    head = entry;
  }

  std::unordered_map<std::string, CacheEntry *> lookup;

  const int cacheCapacity;
  int cacheSize;
  CacheEntry *head;
  CacheEntry *tail;
};
