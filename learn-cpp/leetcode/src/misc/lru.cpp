#include "misc/lru.hpp"
#include <gtest/gtest.h>

void fillCache(int startkey, int numkeys, LRUCache &cache) {
  for (int i = startkey; i < startkey + numkeys; i++) {
    std::string key = "k" + std::to_string(i);
    std::string value = "v" + std::to_string(i);
    cache.write(key, value);
  }
}

TEST(LRUCache, TestIfCached) {
  int cacheCapacity = 10;
  LRUCache cache(cacheCapacity);
  fillCache(0, cacheCapacity, cache);
  // all of them should be available
  for (int i = 0; i < cacheCapacity; i++) {
    std::string key = "k" + std::to_string(i);
    std::string value = "v" + std::to_string(i);
    EXPECT_EQ(cache.isCached(key), true);
    EXPECT_STREQ(cache.read(key).c_str(), value.c_str());
  }
}

TEST(LRUCache, TestElimination) {
  int cacheCapacity = 10;
  LRUCache cache(cacheCapacity);
  fillCache(0, cacheCapacity + 1, cache);
  EXPECT_EQ(cache.isCached("k0"), false);
  for (int i = 1; i < cacheCapacity + 1; i++) {
    std::string key = "k" + std::to_string(i);
    std::string value = "v" + std::to_string(i);
    EXPECT_EQ(cache.isCached(key), true);
    EXPECT_STREQ(cache.read(key).c_str(), value.c_str());
  }
}
