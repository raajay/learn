#include "misc/permutations.hpp"
#include <gtest/gtest.h>

TEST(TestPermutations, SortTest1) {
  std::vector<int> a = {5, 3, 5, 1, 4};
  std::vector<int> b = {1, 3, 4, 5, 5};
  std::sort(a.begin(), a.end());
  for (int i = 0; i < 5; i++) {
    EXPECT_EQ(a[i], b[i]);
  }
}

TEST(TestPermutations, DisplayOutDistinct) {
  std::vector<int> a = {1, 2, 3, 4, 5};
  EXPECT_EQ(printPermuations(a), 120);
}

TEST(TestPermutations, DisplayOutDuplicates) {
  std::vector<int> a = {1, 2, 3, 4, 4};
  EXPECT_EQ(printPermuations(a), 60);
}
