#include <iomanip>

/*** SHA-1 ***/

class SHA1 {
 public:
  /**
   * Constructor.
   */
  SHA1() {}

  /**
   * Update the hash using stream of characters.
   */
  void update(char *msg, int msglen) {}

  /**
   * Finalize the hash. Cannot be further updated.
   */
  void finalize() {}
};
