#include "misc/sequences.hpp"
#include <gtest/gtest.h>

TEST(MiscSequence, NumContiguousSumTarget1) {
  std::vector<int> a = {1, 1, 1, 1, 1, 1};
  EXPECT_EQ(findNumContiguousSum(a, 3), findNumContiguousSumBrute(a, 3));
}

TEST(MiscSequence, NumContiguousSumTarget2) {
  std::vector<int> a = {-1, 1, 2, -2, -3, 1, 2};
  EXPECT_EQ(findNumContiguousSum(a, 0), findNumContiguousSumBrute(a, 0));
}

TEST(MiscSequence, LongestIncreasingSubsequence1) {
  std::vector<int> a = {1, 0, 2, 0, 0, 3};
  EXPECT_EQ(longestIncreasingSubsequence(a), 3);
}

TEST(PivotSequence, Case1) {
  std::vector<int> a = {4, 5, 7, 2, 1, 0, -9};
  int pivot_key = 2;
  int loc = pivot(a, pivot_key);
  for (int i = 0; i < a.size(); i++) {
    if (i < loc) {
      EXPECT_LT(a[i], pivot_key);
    } else {
      EXPECT_GE(a[i], pivot_key);
    }
  }
}

TEST(PivotSequence, CaseAllEqualToPivotKey) {
  int pivot_key = 5;
  std::vector<int> a(10, pivot_key);
  int loc = pivot(a, pivot_key);
  EXPECT_EQ(loc, 0);
}

TEST(PivotSequence, CaseAllGreaterToPivot) {
  int pivot_key = 5;
  std::vector<int> a(10, 10);
  int loc = pivot(a, pivot_key);
  EXPECT_EQ(loc, 0);
}

TEST(NumTriangles, CaseBruteForceSanity) {
  std::vector<int> a = {1, 2, 3, 4};
  EXPECT_EQ(findNumTrianglesBrute(a), 1);
}

TEST(NumTriangles, CaseIncreasing) {
  std::vector<int> a = {1, 2, 3, 4};
  EXPECT_EQ(findNumTriangles(a), findNumTrianglesBrute(a));
}

TEST(NumTriangles, CaseEqual) {
  std::vector<int> a(10, 10);
  EXPECT_EQ(findNumTriangles(a), findNumTrianglesBrute(a));
}

TEST(NumTriangles, Case2) {
  std::vector<int> a = {3, 65, 1, 4, 7, 3, 23, 10, 8, 9};
  EXPECT_EQ(findNumTriangles(a), findNumTrianglesBrute(a));
}

TEST(LongestCommonSubsequence, CaseGeeksForGeeks) {
  EXPECT_EQ(lengthCommonSubsequence("ABCDGH", "AEDFHR"), 3);
  EXPECT_EQ(lengthCommonSubsequence("AGGTAB", "GXTXAYB"), 4);
}

TEST(LongestRepeatedSequence, CaseGeeksForGeeks) {
  EXPECT_EQ(lengthLongestRepeatedSequence("AABEBCDD"), 3);
  EXPECT_EQ(lengthLongestRepeatedSequence("AXAYBXBYCXCYDD"), 4);
  EXPECT_EQ(lengthLongestRepeatedSequence("ABCDEFGHIJABCDEFGHIJ"), 10);
}

TEST(MaximumContiguousSubarray, Case0) {
  std::vector<int> a = {1, 2, 3, 4, 5};
  EXPECT_EQ(findMaxContiguousSequence(a), 15);
}

TEST(MaximumContiguousSubarray, Case1) {
  std::vector<int> a = {1, 2, 3, -4, 5};
  EXPECT_EQ(findMaxContiguousSequence(a), 7);
}

TEST(MaximumContiguousSubarray, Case2) {
  std::vector<int> a = {-1, 2, 3, -4, 5};
  EXPECT_EQ(findMaxContiguousSequence(a), 6);
}

TEST(MaximumContiguousSubarray, Case3) {
  std::vector<int> a = {-1, 2, 3, -4, -5};
  EXPECT_EQ(findMaxContiguousSequence(a), 5);
}
