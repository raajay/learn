#include "misc/concurrency.hpp"
#include <gtest/gtest.h>

TEST(Concurrency, NullLock) {
  MyLock lock;
  EXPECT_EQ(true, isThereRaceCondition(&lock));
}

TEST(Concurrency, DumbSpinLock) {
  MyDumbSpinLock lock;
  EXPECT_EQ(true, isThereRaceCondition(&lock));
}

TEST(Concurrency, SpinLock) {
  MySpinLock lock;
  EXPECT_EQ(false, isThereRaceCondition(&lock));
}

TEST(Concurrency, SpinLockYield) {
  MySpinLockYield lock;
  EXPECT_EQ(false, isThereRaceCondition(&lock));
}
