#include <iostream>
#include <vector>

void preprocessKMP(std::string s, std::vector<int> &shift) {
  int last_matched = 1, prefix_end = 0;
  while (last_matched < s.length()) {
    if (s[last_matched] == s[prefix_end]) {
      shift[last_matched] = prefix_end + 1;
      last_matched++;
      prefix_end++;
    } else {
      if (prefix_end == 0) {
        shift[last_matched] = 0;
        last_matched++;
      } else {
        prefix_end = shift[prefix_end - 1];
      }
    }
  }
}

std::vector<int> kmp(std::string text, std::string word) {
  std::vector<int> retval;
  std::vector<int> shift(word.length(), 0);
  preprocessKMP(word, shift);
  int j = 0, i = 0;
  while (i < text.length()) {
    if (text[i] == word[j]) {
      i++;
      j++;
      if (j == word.length()) {
        retval.push_back(i - j);
        j = shift[j - 1];
      }
    } else {
      if (0 == j) {
        // first chr did not match
        i++;
      } else {
        j = shift[j - 1];
      }
    }
  }
  return retval;
}

std::vector<int> boyerMoore(std::string sentence, std::string word) {
  std::vector<int> retval;
  // for each i, find the smallest j, such that word[j..i] = word[1 .. i -j]
  // if we can store the smallest j for each i, then we can easily skip by j
  // upon failure
  return retval;
}
