#include <atomic>
#include <iostream>
#include <thread>
#include <vector>

class MyLock {
 public:
  virtual void lock() {}
  virtual void unlock() {}
};

class MyDumbSpinLock : public MyLock {
 public:
  MyDumbSpinLock() : flag(false) {}
  virtual void lock() override {
    while (flag == true)
      ;
    flag = true;
  }
  virtual void unlock() override { flag = false; }

 private:
  bool flag;
};

class MySpinLock : public MyLock {
 public:
  MySpinLock() : flag(false) {}
  virtual void lock() override {
    while (flag.test_and_set(std::memory_order_relaxed) == 1)
      ;
  }

  virtual void unlock() override { flag.clear(std::memory_order_relaxed); }

 protected:
  std::atomic_flag flag;
};

class MySpinLockYield : public MySpinLock {
 public:
  MySpinLockYield() : MySpinLock() {}
  virtual void lock() override {
    while (flag.test_and_set(std::memory_order_relaxed) == 1) {
      std::this_thread::yield();
    }
  }
};

void increment100k(int K, int *a, MyLock *lock) {
  for (int i = 0; i < K; i++) {
    lock->lock();
    (*a)++;
    lock->unlock();
  }
}

bool isThereRaceCondition(MyLock *lock) {
  int a = 0;
  int num_iters = 100000;
  int num_threads = 5;
  std::vector<std::thread> threads;
  for (int i = 0; i < num_threads; i++) {
    threads.push_back(std::thread(increment100k, num_iters, &a, lock));
  }
  for (auto &th : threads) {
    th.join();
  }
  std::cout << a << std::endl;
  return a < num_threads * num_iters;
}
