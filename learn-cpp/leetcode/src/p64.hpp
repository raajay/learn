#include <iostream>
#include <vector>

class p64Solution {
 public:
  int minPathSum(std::vector<std::vector<int>> &grid) {
    int numRows = grid.size();
    if (numRows == 0) {
      return 0;
    }
    int numCols = grid[0].size();

    if (numCols == 0) {
      return 0;
    }

    std::vector<std::vector<int>> memory;
    for (int i = 0; i < numRows; i++) {
      memory.push_back(std::vector<int>());
    }

    // fix the top row
    int s = 0;
    for (int i = 0; i < numCols; i++) {
      s += grid[0][i];
      memory[0].push_back(s);
    }

    // fix the first column
    s = grid[0][0];
    for (int i = 1; i < numRows; i++) {
      s += grid[i][0];
      memory[i].push_back(s);
    }

    // now the rest
    for (int i = 1; i < numRows; i++) {
      for (int j = 1; j < numCols; j++) {
        memory[i].push_back(grid[i][j] +
                            std::min(memory[i][j - 1], memory[i - 1][j]));
      }
    }

    return memory[numRows - 1][numCols - 1];
  }
};
