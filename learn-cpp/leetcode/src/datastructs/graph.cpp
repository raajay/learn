#include "datastructs/graph.hpp"
#include <gtest/gtest.h>

class GraphTest : public ::testing::Test {
 protected:
  std::unordered_map<int, std::vector<int>> chain4 = {{1, {2}},
                                                      {2, {3}},
                                                      {3, {4}},
                                                      {4, {}}};
};

TEST_F(GraphTest, GraphCreation) {
  Graph g(chain4);
  EXPECT_EQ(g.nodes().size(), 4);
}

TEST_F(GraphTest, DfsChainTest) {
  Graph g(chain4);
  std::vector<int> output;
  dfs(g.nodes(std::vector<int>({1, 2, 3, 4})), output);
  EXPECT_EQ(output.size(), 4);
  for (int i = 0; i < 4; i++) {
    EXPECT_EQ(i + 1, output[i]);
  }
}

TEST_F(GraphTest, BfsChainTest) {
  Graph g(chain4);
  std::vector<int> output;
  dfs(g.nodes(std::vector<int>({1, 2, 3, 4})), output);
  EXPECT_EQ(output.size(), 4);
  for (int i = 0; i < 4; i++) {
    EXPECT_EQ(i + 1, output[i]);
  }
}
