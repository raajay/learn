#include <cassert>

class DLLNode {
 public:
  void* payload;
  DLLNode* next;
  DLLNode* prev;
};

void insertAtHead(DLLNode*& head, DLLNode*& tail, DLLNode* node) {
  if (nullptr == tail)
    tail = node;
  assert(head);
  head->prev = node;
  node->next = head;
  head = node;
}

void unlink(DLLNode*& head, DLLNode*& tail, DLLNode* node) {}
