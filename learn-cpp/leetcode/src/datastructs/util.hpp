#ifndef UTIL_HPP
#define UTIL_HPP

#include <iostream>
#include <vector>

void displayVec(std::vector<int> v) {
  for (auto x : v) {
    std::cout << " " << x;
  }
  std::cout << std::endl;
}

#endif
