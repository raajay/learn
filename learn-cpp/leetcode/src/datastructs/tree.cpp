#include "datastructs/tree.hpp"
#include <iostream>

TreeNode *addNode(TreeNode *root, int val) {
  if (nullptr == root) {
    return new TreeNode(val);
  }
  if (val < root->val) {
    root->left = addNode(root->left, val);
  } else {
    root->right = addNode(root->right, val);
  }
  return root;
}

TreeNode *createBST(std::vector<int> &elements) {
  TreeNode *root = nullptr;
  for (auto e : elements) {
    root = addNode(root, e);
  }
  return root;
}

void destroyBST(TreeNode *root) {
  if (nullptr == root) {
    return;
  }
  destroyBST(root->left);
  destroyBST(root->right);
  delete root;
}

void printInOrder(TreeNode *root) {
  if (nullptr == root)
    return;
  printInOrder(root->left);
  std::cout << " " << root->val;
  printInOrder(root->right);
}

void inOrder(TreeNode *root, std::vector<TreeNode *> &nodes) {
  if (nullptr == root)
    return;
  inOrder(root->left, nodes);
  nodes.push_back(root);
  inOrder(root->right, nodes);
}
