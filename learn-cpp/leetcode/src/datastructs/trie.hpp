#include <string>
#include <unordered_map>
#include <vector>

class TrieNode {
 public:
  TrieNode() : isWordEnd(false) {}
  std::unordered_map<char, TrieNode *> children;
  bool isWordEnd;
};

/**
 * Iterative algorithm to add a word to the trie
 */
void addWord(TrieNode *root, std::string s) {
  for (int i = 0; i < s.length(); i++) {
    if (root->children.find(s[i]) == root->children.end()) {
      // create a new node, and let children[s[i]] point to this
      root->children[s[i]] = new TrieNode();
    }
    root = root->children[s[i]];
  }
  root->isWordEnd = true;
}

bool findWord(TrieNode *root, std::string s) {
  int i = 0;
  while (i < s.length() && nullptr != root) {
    char c = s[i];
    root = (root->children.find(c) == root->children.end()) ? nullptr
                                                            : root->children[c];
    i++;
  }
  return root && root->isWordEnd;
}

void destroyTrie(TrieNode *root) {
  for (auto &entry : root->children) {
    destroyTrie(entry.second);
  }
  delete root;
}
