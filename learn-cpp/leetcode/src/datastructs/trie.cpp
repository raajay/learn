#include "datastructs/trie.hpp"
#include <ctime>
#include <fstream>
#include <gtest/gtest.h>
#include <iostream>

#define NUM_WORDS 370098

class TrieTest : public ::testing::Test {
 public:
  static TrieNode* createTrie(std::vector<std::string> words) {
    TrieNode* root = new TrieNode();
    for (auto w : words) {
      addWord(root, w);
    }
    return root;
  }

  static void readLargeFile(std::vector<std::string>& words) {
    std::ifstream large_file("../data/words_alpha.txt");
    std::string s;
    if (!large_file.is_open()) {
      std::cout << "File is not open." << std::endl;
      return;
    }
    while (!large_file.eof()) {
      large_file >> s;
      if (s.length() == 0)
        continue;
      words.push_back(s);
    }
    large_file.close();
  }

  static TrieNode* createLargeTrie() {
    std::vector<std::string> words;
    readLargeFile(words);
    return TrieTest::createTrie(words);
  }

  static void createLargeHashMap(std::unordered_map<std::string, bool> mm) {
    std::vector<std::string> words;
    readLargeFile(words);
    mm.clear();
    for (auto& w : words) {
      mm[w] = true;
    }
  }
};

TEST_F(TrieTest, AddAndFind) {
  TrieNode* root = TrieTest::createTrie(std::vector<std::string>(
      {"apple", "application", "apple", "banana", "guava"}));
  EXPECT_EQ(findWord(root, "banana"), true);
  EXPECT_EQ(findWord(root, "pineapple"), false);
  std::clock_t start;
  double elapsed = (std::clock() - start) / (double)CLOCKS_PER_SEC;
}

/** Performance test
TEST_F(TrieTest, ScaleTestTrie) {
  std::clock_t start = std::clock();
  auto root = TrieTest::createLargeTrie();
  double elapsed = (std::clock() - start) / (double)CLOCKS_PER_SEC;
  std::cout << elapsed << std::endl;
}

TEST_F(TrieTest, ScaleTestHashMap) {
  std::unordered_map<std::string, bool> m;
  std::clock_t start = std::clock();
  TrieTest::createLargeHashMap(m);
  double elapsed = (std::clock() - start) / (double)CLOCKS_PER_SEC;
  std::cout << elapsed << std::endl;
}
*/
