#include <deque>
#include <iostream>
#include <stack>
#include <unordered_map>
#include <vector>

class GraphNode {
 public:
  GraphNode(int k) : key(k), visited(false) {}
  int key;
  std::vector<GraphNode*> neighbors;
  bool visited;
  bool marked_for_visit;
};

class Graph {
 public:
  /**
   * Construct a graph using a map of int to vector of ints which represents the
   * adjacency list.
   */
  Graph(std::unordered_map<int, std::vector<int>> g) {
    for (auto entry : g) {
      GraphNode* node = new GraphNode(entry.first);
      vertices[entry.first] = node;
    }
    for (auto entry : g) {
      auto srcNode = vertices[entry.first];
      for (auto v : entry.second) {
        auto dstNode = vertices[v];
        srcNode->neighbors.push_back(dstNode);
      }
    }
  }

  ~Graph() {
    for (auto entry : vertices) {
      delete entry.second;
    }
  }

  std::vector<GraphNode*> nodes() {
    std::vector<GraphNode*> retval;
    for (auto entry : vertices) {
      retval.push_back(entry.second);
    }
    return retval;
  }

  std::vector<GraphNode*> nodes(std::vector<int> keys) {
    std::vector<GraphNode*> retval;
    for (auto k : keys) {
      retval.push_back(vertices[k]);
    }
    return retval;
  }

 private:
  std::unordered_map<int, GraphNode*> vertices;
};

void dfs(const std::vector<GraphNode*>& vertices, std::vector<int>& output) {
  std::stack<GraphNode*> ss;
  for (auto v : vertices) {
    if (v->visited) {
      continue;
    }
    ss.push(v);
    while (!ss.empty()) {
      GraphNode* curr = ss.top();
      ss.pop();
      output.push_back(curr->key);
      curr->visited = true;
      for (auto n : curr->neighbors) {
        if (n->visited || n->marked_for_visit)
          continue;
        n->marked_for_visit = true;
        ss.push(n);
      }
    }
  }
}

void bfs(const std::vector<GraphNode*>& vertices, std::vector<int>& output) {
  std::deque<GraphNode*> q;
  for (auto v : vertices) {
    if (v->visited)
      continue;
    q.push_back(v);
    while (!q.empty()) {
      GraphNode* curr = q.front();
      q.pop_front();
      output.push_back(curr->key);
      curr->visited = true;
      for (auto n : curr->neighbors) {
        if (n->visited || n->marked_for_visit)
          continue;
        n->marked_for_visit = true;
        q.push_back(n);
      }
    }
  }
}
