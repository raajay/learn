#ifndef TREE_H
#define TREE_H

#include <gtest/gtest.h>
#include <vector>

struct TreeNode {
  int val;
  TreeNode *left;
  TreeNode *right;
  TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
};

TreeNode *addNode(TreeNode *root, int val);
TreeNode *createBST(std::vector<int> &elements);
void destroyBST(TreeNode *root);
void printInOrder(TreeNode *root);
void inOrder(TreeNode *root, std::vector<TreeNode *> &nodes);

template <typename T>
void EXPECT_VEC_EQ(const std::vector<T> &a, const std::vector<T> &b) {
  EXPECT_EQ(a.size(), b.size());
  int n = std::min(a.size(), b.size());
  for (int i = 0; i < n; i++) {
    EXPECT_EQ(a[i], b[i]);
  }
}

#endif
