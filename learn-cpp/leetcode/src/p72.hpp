// read:https://web.stanford.edu/class/cs124/lec/med.pdf

#include <iostream>
#include <vector>

class p72Solution {
 public:
  int minEditDistance(std::string word1, std::string word2) {
    int n = word1.length();
    int m = word2.length();
    // a single line instantiation of 2-D vec array
    std::vector<std::vector<int>> memory(n + 1, std::vector<int>(m + 1, 0));

    for (int i = 0; i <= n; i++) {
      memory[i][0] = i;
    }

    for (int j = 0; j <= m; j++) {
      memory[0][j] = j;
    }

    for (int i = 1; i <= n; i++) {
      for (int j = 1; j <= m; j++) {
        int min_insert_delete =
            std::min(memory[i - 1][j], memory[i][j - 1]) + 1;
        int min_replace =
            ((word2[j - 1] == word1[i - 1]) ? 0 : 1) + memory[i - 1][j - 1];
        memory[i][j] = std::min(min_insert_delete, min_replace);
      }
    }

    return memory[n][m];
  }
};
