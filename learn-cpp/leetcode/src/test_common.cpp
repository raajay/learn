#include "datastructs/tree.hpp"
#include <gtest/gtest.h>

TEST(TreeCreation, NullTree) {
  std::vector<int> elements;
  auto root = createBST(elements);
  EXPECT_EQ(nullptr, root);
}

TEST(TreeCreation, SingleElement) {
  std::vector<int> elements{1};
  auto root = createBST(elements);
  EXPECT_NE(nullptr, root);
  EXPECT_EQ(root->val, 1);
  EXPECT_EQ(root->left, nullptr);
  EXPECT_EQ(root->right, nullptr);
}

TEST(TreeCreation, TwoElement) {
  std::vector<int> elements = {2, 1};
  auto root = createBST(elements);
  EXPECT_NE(nullptr, root);
  EXPECT_EQ(root->val, 2);
  EXPECT_NE(root->left, nullptr);
}
