/**
 * Given an array of struct nodes, check if all of the nodes are part of a
 * single binary tree and if all nodes of that binary tree are in this array or
 * not.
 * Asked me to code the entire problem too.
 */

#include "datastructs/tree.hpp"
#include <algorithm>
#include <deque>
#include <gtest/gtest.h>
#include <queue>
#include <unordered_map>
#include <vector>

bool isTree(const std::vector<TreeNode *> &nodes) {
  std::unordered_map<TreeNode *, int> components;
  // Init all the nodes to unknown components
  for (auto n : nodes) {
    components[n] = -2;
  }

  for (int i = 0; i < nodes.size(); i++) {
    // i - is the root
    if (components[nodes[i]] != -2) {
      continue;
    }
    // start traversing with current node as the root; use any traversal
    std::deque<TreeNode *> qq;
    qq.push_back(nodes[i]);
    while (!qq.empty()) {
      auto curr_node = qq.front();
      qq.pop_front();
      if (components.find(curr_node) == components.end()) {
        // traversed node is not in array
        return false;
      } else if (components[curr_node] != -2) {
        // it already has a component assigned, update it
        components[curr_node] = i;
        continue;  // do not traverse further
      }
      // un observed node
      components[curr_node] = i;
      if (curr_node->left != nullptr) {
        qq.push_back(curr_node->left);
      }
      if (curr_node->right != nullptr)
        qq.push_back(curr_node->right);
    }
  }

  // there should be only one node which belongs to its own component (the
  // root)
  int count_root = 0;
  for (int i = 0; i < nodes.size(); i++) {
    int component = components[nodes[i]];
    if (component == i) {
      count_root++;
    }
  }
  return 1 == count_root;
}

/* Testing */
// 1. Construct a tree
// 2. Get all nodes through traversal
// 3. Shuffle the nodes

TEST(UberPastQ1, Case0) {
  std::vector<int> values = {1, 2, 3, 4, 5, 6, 7, 8, 9};
  auto root = createBST(values);
  std::vector<TreeNode *> nodes;
  inOrder(root, nodes);
  std::random_shuffle(nodes.begin(), nodes.end());
  EXPECT_EQ(true, isTree(nodes));
}

TEST(UberPastQ1, Case1) {
  std::vector<int> values = {1, 2, 3, 4, 5, 6, 7, 8, 9};
  auto root = createBST(values);
  std::vector<TreeNode *> nodes;
  inOrder(root, nodes);
  std::random_shuffle(nodes.begin(), nodes.end());
  nodes.pop_back();
  nodes.pop_back();
  EXPECT_EQ(false, isTree(nodes));
}

TEST(UberPastQ1, Case2) {
  std::vector<int> values = {1, 2, 3, 4, 5, 6, 7, 8, 9};
  auto root = createBST(values);
  auto root2 = createBST(values);
  std::vector<TreeNode *> nodes;
  inOrder(root, nodes);
  std::random_shuffle(nodes.begin(), nodes.end());
  inOrder(root2, nodes);
  EXPECT_EQ(false, isTree(nodes));
}
