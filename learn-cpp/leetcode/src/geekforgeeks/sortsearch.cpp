#include <cassert>
#include <cstdlib>
#include <vector>

int partition(std::vector<int> &nums, int start, int end, int pivot) {}

int findKLargest(std::vector<int> &nums, int k) {
  int start = 0, end = nums.size() - 1;
  assert(k <= nums.size() && k > 0);
  while (true) {
    int ridx = rand() % (end - start + 1) + start;
    int loc = partition(nums, start, end, nums[ridx]);
    if (loc == k) {
      // this is the final case and is guaranteed to arrive here.
      return nums[loc];
    } else if (loc < k) {
      start = loc + 1;
    } else {
      end = loc - 1;
    }
  }
}
