/**
 * Given a Binary Tree, find diameter of it.  The diameter of a tree is the
 * number of nodes on the longest path between two leaves in the tree.
 */
#include "datastructs/tree.hpp"
#include <gtest/gtest.h>
#include <vector>

void treeDiameter0(TreeNode *root, int &height, int &diameter) {
  if (root == nullptr) {
    height = 0;
    diameter = 0;
    return;
  }
  int leftheight, rightheight;
  int leftdiameter, rightdiameter;
  treeDiameter0(root->left, leftheight, leftdiameter);
  treeDiameter0(root->right, rightheight, rightdiameter);
  height = 1 + std::max(leftheight, rightheight);
  diameter = std::max(std::max(rightdiameter, leftdiameter),
                      rightheight + leftheight + 1);
}

int treeDiameter(TreeNode *root) {
  int height;
  int diameter;
  treeDiameter0(root, height, diameter);
  return diameter;
}

TEST(TreeDiameter, CaseNullTree) { EXPECT_EQ(0, treeDiameter(nullptr)); }

TEST(TreeDiameter, CaseSingleNode) {
  TreeNode *root = new TreeNode(0);
  EXPECT_EQ(1, treeDiameter(root));
}
