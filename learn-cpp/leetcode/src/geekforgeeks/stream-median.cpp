/**
 * Given an input stream of n integers the task is to insert integers to stream
 * and print the median of the new stream formed by each insertion of x to the
 * stream.
 */

#include <algorithm>
#include <cassert>
#include <cmath>
#include <datastructs/util.hpp>
#include <gtest/gtest.h>
#include <vector>

bool minHeapComparator(const int &a, const int &b) { return a > b; }

class StreamBuffer {
 public:
  StreamBuffer() {}

  void insert(int x) {

    if (left_heap.size() > 0 && x <= left_heap.front()) {
      left_heap.push_back(x);
      std::push_heap(left_heap.begin(), left_heap.end());
    } else {
      right_heap.push_back(x);
      std::push_heap(right_heap.begin(), right_heap.end(), minHeapComparator);
    }

    if (left_heap.size() > right_heap.size() + 1) {
      // extract max from left heap and push it into the min heap for the rhs
      std::pop_heap(left_heap.begin(), left_heap.end());
      int y = left_heap.back();
      left_heap.pop_back();
      // push into the right heap
      right_heap.push_back(y);
      std::push_heap(right_heap.begin(), right_heap.end(), minHeapComparator);
    }

    if (right_heap.size() > left_heap.size() + 1) {
      // extract min from right heap and push it into the left heap
      std::pop_heap(right_heap.begin(), right_heap.end(), minHeapComparator);
      int y = right_heap.back();
      right_heap.pop_back();
      // push into the left heap
      left_heap.push_back(y);
      std::push_heap(left_heap.begin(), left_heap.end());
    }

    assert(right_heap.size() == left_heap.size() ||
           right_heap.size() == 1 + left_heap.size() ||
           left_heap.size() == 1 + right_heap.size());
  }

  int median() {
    if (left_heap.size() == right_heap.size()) {
      return (left_heap.front() + right_heap.front()) / 2;
    } else if (left_heap.size() > right_heap.size()) {
      return left_heap.front();
    } else {
      return right_heap.front();
    }
  }

 private:
  std::vector<int> left_heap;   // has the smaller half
  std::vector<int> right_heap;  // size of right heap is always equal or one
                                // less than left_heap
};

TEST(StreamMedian, Case0) {
  std::vector<int> s = {5, 15, 1, 3};
  std::vector<int> e = {5, 10, 5, 4};
  StreamBuffer b;
  for (int i = 0; i < s.size(); i++) {
    b.insert(s[i]);
    EXPECT_EQ(b.median(), e[i]);
  }
}

int bruteForceMedian(std::vector<int> t) {
  std::sort(t.begin(), t.end());
  if (t.size() % 2 == 1) {
    // median is mid
    return t[t.size() / 2];
  } else {
    return (t[t.size() / 2] + t[t.size() / 2 - 1]) / 2;
  }
}

TEST(StreamMedian, Large0) {
  int n = 1000;
  std::vector<int> s, e;
  for (int i = 0; i < n; i++) {
    int x = rand() % 10000;
    s.push_back(x);
    e.push_back(bruteForceMedian(s));
  }

  // displayVec(s);
  // displayVec(e);

  StreamBuffer b;
  for (int i = 0; i < n; i++) {
    b.insert(s[i]);
    EXPECT_EQ(e[i], b.median());
  }
}
