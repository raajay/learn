/**
 * There are N trees in a circle. Each tree has a fruit value associated with
 * it.
 *
 * A bird has to sit on a tree for 0.5 sec to gather all the fruits present on
 * the tree and then the bird can move to a neighboring tree. It takes the bird
 * 0.5 seconds to move from one tree to another. Once all the fruits are picked
 * from a particular tree, she can’t pick any more fruits from that tree. The
 * maximum number of fruits she can gather is infinite.
 *
 * We are given N and M (the total number of seconds the bird has), and the
 * fruit values of the trees. We have to maximize the total fruit value that the
 * bird can gather. The bird can start from any tree.
 */
