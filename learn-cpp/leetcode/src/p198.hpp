#include <iostream>
#include <vector>

class p198Solution {
 public:
  int rob(std::vector<int>& nums) {
    int n = nums.size();
    int loot = 0;
    int prev1 = 0;
    int prev2 = 0;

    for (int i = 0; i < n; i++) {
      loot = std::max(nums[i] + prev2, prev1);
      prev2 = prev1;
      prev1 = loot;
    }
    return loot;
  }
};
