#include "datastructs/tree.hpp"
#include <iostream>

class p230Solution {
 public:
  int kthSmallest(TreeNode *root, int k) {
    int index = 0;
    bool found = false;
    return inorder(root, index, found, k);
  }

  int inorder(TreeNode *root, int &index, bool &found, int k) {
    if (root->left) {
      int lv = inorder(root->left, index, found, k);
      if (found) {
        return lv;
      }
    }

    index++;
    if (k == index) {
      found = true;
      return root->val;
    }

    if (root->right) {
      int rv = inorder(root->right, index, found, k);
      if (found) {
        return rv;
      }
    }

    return -1;
  }
};
