#include <iostream>

class p726Solution {
 public:
  struct Tuple {
    std::string element;
    int count;
    Tuple(std::string s) : element(s), count(0) {}
    virtual bool isLeftBrace() { return false; }
  };

  struct LeftBrace : Tuple {
    LeftBrace() : Tuple("(") {}
    virtual bool isLeftBrace() { return true; }
    virtual bool isRightBrace() { return false; }
  };

  struct RightBrace : Tuple {
    RightBrace() : Tuple(")") {}
    virtual bool isRightBrace() { return true; }
    virtual bool isLeftBrace() { return false; }
  };

  std::string countOfAtoms(std::string formula) {}
};
