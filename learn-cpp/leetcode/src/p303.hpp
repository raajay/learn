#include <iostream>
#include <vector>

class p303Solution {
 public:
  std::vector<int> cumSum;

  p303Solution(std::vector<int> nums) {
    cumSum.push_back(0);
    int s = 0;
    for (int i = 0; i < nums.size(); i++) {
      s += nums[i];
      cumSum.push_back(s);
    }
  }

  int sumRange(int i, int j) { return cumSum[j + 1] - cumSum[i]; }
};
