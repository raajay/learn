#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int main() {
  printf("hello world! pid = %d\n", getpid());
  // create child through fork
  int rc = fork();
  if (rc < 0) {
    printf("fork failed! rc = %d\n", rc);
    exit(1);
  } else if (rc == 0) {
    printf("hello! i am the child process, pid = %d\n", (int)getpid());
  } else  {
    int wc = waitpid((pid_t) rc, NULL, WUNTRACED);
    printf("hello! i am the parent process of %d, pid = %d\n", rc, (int)getpid());
  }
  return 0;
}
