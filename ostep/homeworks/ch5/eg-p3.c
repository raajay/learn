#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>

int main() {
  printf("hello world! pid = %d\n", getpid());
  // create child through fork
  int rc = fork();
  if (rc < 0) {
    printf("fork failed! rc = %d\n", rc);
    exit(1);
  } else if (rc == 0) {
    printf("hello! i am the child process, pid = %d\n", (int)getpid());
    // now we will morph the child to a different process
    char *myargs[3];
    myargs[0] = strdup("wc");
    myargs[1] = strdup("eg-p3.c");
    myargs[2] = NULL;
    execvp(myargs[0], myargs);
    printf("this line should not execute if exec succeeded.\n");
  } else  {
    int wc = wait(NULL);
    printf("hello! i am the parent process of %d, pid = %d\n", rc, (int)getpid());
  }
  return 0;
}
