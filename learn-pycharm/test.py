import os
import numpy as np
import operator
from collections import defaultdict
import matplotlib.pyplot as plt


def get_pdf(times, start=0, pdf=None):
    if pdf is None:
        pdf = defaultdict(lambda : 0)
    for t in times:
        pdf[round(t + start)] += 1
    return pdf


avg_endtime = 500
st_endtime = 100
end_times = avg_endtime + st_endtime * (np.random.randn(10000))
T = 1

times = range(0,T)

pdf = defaultdict(lambda : 0)

for t in times:
    pdf = get_pdf(end_times, t, pdf)

active = defaultdict(lambda : 0)

for t in times:
    active[t] = 10000 * t;

for t in range(1+max(active.keys()), 1000):
    active[t] = 10000 * len(times)


print "alive ", max(active.values())
print "dead  ", sum(pdf.values()

def get_total(active, dead):
    current_dead = 0
    for i in range(0,1000):
        current_dead += dead[i]
        active[i] -= current_dead
    return active

active = get_total(active, pdf)

xx = sorted(active.keys())
yy = [active[x] for x in xx]

fig = plt.figure()
plt.plot(xx,yy)
plt.savefig('test.pdf')


def myfunct():
    return "mydata"


if __name__ == "__main__":
    print "hello world"
    print os.listdir("efgh")

