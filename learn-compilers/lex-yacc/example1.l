%{
#include <stdio.h>
%}

%%
stop|start printf("KEYWORD");
[0-9]+ printf("INTEGER");
[0-9]*\.[0-9]+ printf("FLOAT");
[a-zA-Z]+ printf("WORD");
%%
