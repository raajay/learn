extern "C" {

#include <netlink/netlink.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#define IFLIST_REPLY_BUFFER 8192

// netlink request format
typedef struct netlink_request {
    struct nlmsghdr hdr; // header of the request
    struct rtgenmsg gen; // body of the request
} request_t;


void print_link(struct nlmsghdr* msg) {
    struct ifinfomsg *iface;
    struct rtattr *attribute;
    int len;

    iface = NLMSG_DATA(msg);
    len = msg->nlmsg_len - NLMSG_LENGTH(sizeof(*iface));

    for (attribute = IFLA_RTA(iface); RTA_OK(attribute, len); attribute = RTA_NEXT(attribute, len)) {
        switch(attribute->rta_type) {
            case IFLA_IFNAME:
                printf("Interaface %d: %s\n", iface->ifi_index, (char*) RTA_DATA(attribute));
                break;
            default:
                break;
        }
    }
}

int main() {
    int fd;
    // local address specifier
    struct sockaddr_nl local_addr;
    memset(&local_addr, 0, sizeof(local_addr)); // init all to zero
    local_addr.nl_family = AF_NETLINK;
    pid_t pid = getpid();
    local_addr.nl_pid = pid;
    local_addr.nl_groups = 0; // not a multicast address

    // kernel netlink address specifier
    struct sockaddr_nl kernel_addr;
    memset(&kernel_addr, 0, sizeof(kernel_addr));
    kernel_addr.nl_family = AF_NETLINK;
    kernel_addr.nl_pid = 0;
    kernel_addr.nl_groups = 0;

    // the request data structure
    request_t request;
    memset(&request, 0, sizeof(request));
    request.hdr.nlmsg_len = NLMSG_LENGTH(sizeof(struct rtgenmsg));
    request.hdr.nlmsg_type = RTM_GETLINK;
    request.hdr.nlmsg_flags = NLM_F_REQUEST | NLM_F_DUMP;
    request.hdr.nlmsg_seq = 1;
    request.hdr.nlmsg_pid = pid;
    request.gen.rtgen_family = AF_PACKET;

    // why is this used?
    struct iovec io;
    io.iov_base = &request;
    io.iov_len = request.hdr.nlmsg_len;

    //  i have no idea what this msg is?
    struct msghdr rtnl_msg;
    memset(&rtnl_msg, 0, sizeof(rtnl_msg));
    rtnl_msg.msg_iov = &io;
    rtnl_msg.msg_iovlen = 1;
    rtnl_msg.msg_name = &kernel_addr;
    rtnl_msg.msg_namelen = sizeof(kernel_addr);


    fd = socket(AF_NETLINK, SOCK_RAW, NETLINK_ROUTE);
    if(bind(fd, (struct sockaddr*) &local_addr, sizeof(local_addr)) < 0) {
        printf("Error in binding the netlink socket\n");
        exit(-1);
    }
    printf("Socket bind is successful\n");
    sendmsg(fd, (struct msghdr*) &rtnl_msg, 0);

    // let us now wait for the reply
    int msg_received = 0;
    char reply_buffer[IFLIST_REPLY_BUFFER];
    while(!msg_received) {
        struct nlmsghdr *msg_ptr;
        struct msghdr rtnl_reply;
        memset(&rtnl_reply, 0, sizeof(rtnl_reply));
        struct iovec io_reply;
        memset(&io_reply, 0, sizeof(io_reply));

        io_reply.iov_base = reply_buffer;
        io_reply.iov_len = IFLIST_REPLY_BUFFER;

        rtnl_reply.msg_iov = &io_reply;
        rtnl_reply.msg_iovlen = 1;
        rtnl_reply.msg_name = &kernel_addr;
        rtnl_reply.msg_namelen = sizeof(kernel_addr);

        int len = recvmsg(fd, &rtnl_reply, 0); // read lots of data
        if (len) {
            for(msg_ptr = (struct nlmsghdr*) reply_buffer; NLMSG_OK(msg_ptr, len); msg_ptr = NLMSG_NEXT(msg_ptr, len)) {
                switch(msg_ptr->nlmsg_type) {
                    case NLMSG_DONE:
                        msg_received++; // terminate
                        break;

                    case RTM_NEWLINK:
                        print_link(msg_ptr);
                        break;

                    default:
                        printf("%d", len);
                        break;
                }
            }
        }
    }

    return 0;
}

}
