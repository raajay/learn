#include <stdio.h>
#include <time.h>

typedef struct ListNode {
    int a;
    int b;
    float c;
} node_alias;

int main() {
    clockid_t types[] = { CLOCK_REALTIME, CLOCK_MONOTONIC, CLOCK_PROCESS_CPUTIME_ID, CLOCK_THREAD_CPUTIME_ID, (clockid_t) - 1 };
    struct timespec spec;
    int i;
    for (i = 0; types[i] != (clockid_t) - 1; i++ ) {
        if ( clock_getres( types[i], &spec ) != 0 ) {
            printf( "Timer %d not supported.\n", types[i] );
        } else {
            printf( "Timer: %d, Seconds: %ld Nanos: %ld\n", i, spec.tv_sec, spec.tv_nsec );
        }
    }
    return 0;
}
