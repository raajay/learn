import java.util.*;
import java.math.*;
import static java.lang.Math.*;

public class SegmentsAndPoints {

    class IntervalComparator implements Comparator<Interval> {
        @Override
        public int compare(Interval first, Interval second) {
            if (first._start == second._start)
                return 0;
            else {
                return  (first._start < second._start) ? -1 : 1;
            }
        }
    }

    class Interval {
        public int _start;
        public int _end;

        public Interval(int start, int end) {
            _start = start;
            _end = end;
        }

        public boolean contains(int point) {
            return (point >= _start) && (point <= _end);
        }

    }
	
	public String isPossible(int[] p, int[] l, int[] r) {

        int n = p.length;
        List<Integer> sorted_p = new ArrayList<>();
        for(int i =0; i < n; i++) {
            sorted_p.add(p[i]);
        }
        Collections.sort(sorted_p);

        ArrayList<Interval> sorted_intervals = new ArrayList<>();
        for(int i = 0; i < n; i++) {
            sorted_intervals.add(new Interval(l[i], r[i]));
        }
        Collections.sort(sorted_intervals, new IntervalComparator());

        Set<Integer> taken = new HashSet<>();
        // iterate over the points
        for(int i = 0; i < n; i++) {
            Set<Integer> candidates  = new HashSet<>();
            for(int j = 0; j < n; j++) {
                if(taken.contains(j))
                    continue;
                if(sorted_intervals.get(j)._start <= sorted_p.get(i)) {
                    if(sorted_intervals.get(j).contains(sorted_p.get(i))) {
                        candidates.add(j);
                    }
                } else {
                    break;
                }
            }
            if(candidates.size() == 0)
                return "Impossible";
            // choose the interval with the min
            int rend = 501;
            int best = -1;
            for (int j : candidates) {
                if(sorted_intervals.get(j)._end < rend) {
                    best = j;
                    rend = sorted_intervals.get(j)._end;
                }
            }
            taken.add(best);
        }
		return "Possible";
	}
}
