import java.util.*;
import java.math.*;
import static java.lang.Math.*;

public class TreeAndVertex {
	// we have to find the vertex with the max degree, say k, if we remove that vertex
    // then the number of components is k
	
	public int get(int[] tree) {
        int n = tree.length; // number of edges
        int[] degree = new int[n+1]; // number of vertices = n+1
        for(int i = 0; i < n+1; i++) {
            degree[i] = 0;
        }
        // iterate over all distinct edges
        for(int i = 0; i < n; i++) {
            int src = i + 1;
            int dst = tree[i];
            degree[src]++;
            degree[dst]++;
        }
        // find the max degree
        int maxdeg = 0;
        for (int i = 0; i < n+1; i++) {
            maxdeg = (degree[i] > maxdeg) ? degree[i] : maxdeg;
        }
		return maxdeg;
	}
}
