import java.util.*;
import java.math.*;
import static java.lang.Math.*;

public class BracketSequenceDiv2 {

	public String trim(String s) {
        int start = s.indexOf('(');
        if (start == -1)
            return "";
        int end = s.lastIndexOf(')');
        if (end == -1)
            return "";
        return s.substring(start, end+1);
    }
	
	public int count(String s) {
        s = trim(s); // the number of distinct bracket sequences will be same even after trimming
		return 0;
	}
}
