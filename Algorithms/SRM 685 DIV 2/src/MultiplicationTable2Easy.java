import java.util.*;
import java.math.*;
import static java.lang.Math.*;

public class MultiplicationTable2Easy {
	
	public String isGoodSet(int[] table, int[] t) {
        Double tmp = Math.floor(Math.sqrt(table.length));
        int n = tmp.intValue();
        HashSet<Integer> small_set = new HashSet<>();
        for(int x : t)
            small_set.add(x);

        for(int x : t) {
            for(int y : t) {
                int val = table[(x*n)+y];
                if (!small_set.contains(val)){
                    return "Not Good";
                }
            }
        }
        return "Good";
	}
}
