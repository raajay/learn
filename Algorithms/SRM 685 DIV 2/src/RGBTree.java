import java.util.*;
import java.math.*;
import static java.lang.Math.*;

public class RGBTree {

	private String[] _graph;
    private int _n;
    private HashMap<String, Boolean> _remember;

    private String key(HashSet<Integer> seen, int num_r, int num_g, int num_b) {
        return seen.toString() + num_r+ "_" + num_g + "_" + num_b;
    }


    private boolean isedge(int i, int j) {
        return (_graph[i].charAt(j) != '.');
    }

    private char edge(int i, int j) {
        return _graph[i].charAt(j);
    }

    private boolean exists(HashSet<Integer> seen, int num_r, int num_g, int num_b) {

        String mykey = key(seen, num_r, num_g, num_b);

        if (num_r == 0 && num_b == 0 && num_g == 0 && seen.size() == _n) {
            _remember.put(mykey, true);
            return true;
        }
        if (seen.size() == _n) {
            _remember.put(mykey, false);
            return false;
        }

        if(_remember.containsKey(mykey)) {
            return _remember.get(mykey);
        }


        boolean retval = false;

        for (int src :  seen) {
            for (int dst = 0; dst < _n ; dst++ ) {
                if (seen.contains(dst))
                    continue;
                if (!isedge(src, dst))
                    continue;

                HashSet<Integer> copy = new HashSet<>(seen);
                copy.add(dst);

                char e = edge(src, dst);
                switch(e) {
                    case 'R':
                        if(num_r > 0 && exists(copy, num_r-1, num_g, num_b)) {

                            _remember.put(mykey, true);
                            return true;
                        }
                        break;
                    case 'G':
                        if(num_g > 0 && exists(copy, num_r, num_g-1, num_b)) {
                            _remember.put(mykey, true);
                            return true;
                        }
                        break;
                    case 'B':
                        if(num_b > 0 && exists(copy, num_r, num_g, num_b-1)) {
                            _remember.put(mykey, true);
                            return true;
                        }
                        break;
                }
            }
        }

        // graph is not partitioned
        _remember.put(mykey, false);
        return false;
    }


	public String exist(String[] G) {
        _graph = G;
        _n = G.length;

        _remember = new HashMap<>();

        HashSet<Integer> seen = new HashSet<>();
        seen.add(0);

        if (exists(seen, (_n-1)/3, (_n-1)/3, (_n-1)/3))
            return "Exist";
        else
            return "Does not exist";
	}
}
