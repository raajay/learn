import java.util.*;
import java.math.*;
import static java.lang.Math.*;

public class DoubleWeights {

	private String[] _weight1;
    private String[] _weight2;

    private boolean isedge(int i, int j) {
        return (_weight1[i].charAt(j) != '.');
    }

    private int getw1(int i, int j) {
        return _weight1[i].charAt(j) - '0';
    }

    private int getw2(int i, int j) {
        return _weight2[i].charAt(j) - '0';
    }

	public int minimalCost(String[] weight1, String[] weight2) {
        _weight1 = weight1;
        _weight2 = weight2;
        int n =  weight1.length;
        int BIG = 40000;
        Integer[] dist1 = new Integer[n];
        Integer[] dist2 = new Integer[n];
        Integer[] dist = new Integer[n];

        HashSet<Integer> seen = new HashSet<>();
        HashSet<Integer> not_seen = new HashSet<>();

        // Init the distances
        for(int i = 0; i < n; i++) {
            dist1[i] = BIG; dist2[i] = BIG; dist[i] = BIG;
            not_seen.add(i);
        }
        dist1[0] = 0; dist2[0] = 0; dist[0] = 0; // set distance to self as zero
        seen.add(0);
        not_seen.remove(0);

        while(seen.size() < n) {
            // iterate over edges from seen to not-seen
            for (int src : seen) {
                for (int dst : not_seen) {
                    if (!isedge(src, dst))
                        continue;

                    if (dist[src] == BIG)
                        continue;

                    int new_weight = dist1[src] * getw2(src, dst) + dist2[src] * getw1(src, dst)
                            + dist1[src] * dist2[src] + getw1(src, dst) * getw2(src, dst);

                    dist[dst] = Math.min(new_weight, dist[dst]);
                    dist1[dst] = Math.min(dist1[src] + getw1(src, dst), dist1[dst]);
                    dist2[dst] = Math.min(dist2[src] + getw2(src, dst), dist2[dst]);
                }
            }
            // update seen and not seen
            int idx = -1;
            int min_dist = BIG;
            for(int i : not_seen) {
                if(dist[i] <= min_dist) {
                    idx = i;
                    min_dist = dist[i];
                }
            }
            assert idx != -1;
            seen.add(idx);
            not_seen.remove(idx);
        }

        if(dist[1] > BIG - 1)
            return -1;
        else
            return dist[1];
	}
}
