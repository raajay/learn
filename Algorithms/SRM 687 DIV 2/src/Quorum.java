import java.util.*;
import java.math.*;
import static java.lang.Math.*;

public class Quorum {
	
	public int count(int[] arr, int k) {
		ArrayList<Integer> s_quorum = new ArrayList<>();
        for(int i = 0; i < arr.length; i++) {
            s_quorum.add(arr[i]);
        }
        Collections.sort(s_quorum);
        int total = 0;
        for(int i = 0; i < k; i++) {
            total += s_quorum.get(i);
        }
		return total;
	}
}
