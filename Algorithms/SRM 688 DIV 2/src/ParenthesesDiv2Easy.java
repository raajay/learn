import java.util.*;
import java.math.*;
import static java.lang.Math.*;

public class ParenthesesDiv2Easy {

    public ArrayList<String> _stack = new ArrayList<>();

	public String trim(String s) {
		int start = s.indexOf('(');
        if (start == -1) {
            return "";
        }
		int end = s.lastIndexOf(')');

        if(end == -1)
            return "";

        if (end < start)
            return "";

		return s.substring(start, end + 1);
	}
	
	public int getDepth(String s) {
        return bruteForce(s);
        /*
		s = trim(s);
        _stack.add(s);
		if (s.length() == 0)
			return 0;

		if (s.length() == 2)
			return 1;

		if(s.charAt(1) == ')') {
			return Math.max(1, getDepth(s.substring(2)));
		} else {
			return 1 + getDepth(s.substring(1, s.length() - 1));
		}
		*/
	}

    public int bruteForce(String s) {
        int max_depth = 0;
        int counter = 0;
        for(int i = 0; i < s.length(); i++) {
            if(s.charAt(i) == '(') {
                counter++;
                max_depth = Math.max(max_depth, counter);
            } else {
                counter--;
            }
        }
        return max_depth;
    }
}
