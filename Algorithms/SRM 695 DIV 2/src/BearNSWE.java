import java.util.*;
import java.math.*;
import static java.lang.Math.*;

public class BearNSWE {
	
	public double totalDistance(int[] a, String dir) {
        int right = 0;
        int up = 0;
        int forward = 0;
		for(int i = 0; i < a.length; i++) {
            forward += a[i];
            switch(dir.charAt(i)) {
                case 'N':
                    up += a[i];
                    break;
                case 'S':
                    up -= a[i];
                    break;
                case 'E':
                    right += a[i];
                    break;
                case 'W':
                    right -= a[i];
                default:
                    break;
            }
        }
		return forward + Math.sqrt(right*right + up*up);
	}
}
